<footer>
    <div class="container">

        <div class="footer gradient">

            <div class="row-fluid">
                <div class="col-md-2"><h4>Browse Apps</h4>
                    <p><a href="/app/agerange/1">Ages 0 – 5</a><br>
                        <a href="/app/agerange/2">Ages: 5 – 8</a><br>
                        <a href="/app/agerange/3">Ages: 8 – 12</a><br>
                        <a href="/app/agerange/5">Ages: 12 +</a><br>
                        <a href="/app/agerange/all">All Ages</a><br>
                    {{--<a href="#">Browse Categories</a></p>--}}
                </div>
                <div class="col-md-2"><h4>Browse Apps</h4>
                    <p>
                        <a href="/featured-apps">Featured Apps</a><br>
                        <a href="/new-apps">Newest Apps</a><br>
                        {{--<a href="#">Highest Rated Apps</a>--}}
                    </p>
                </div>
                <div class="col-md-2"><h4>Account</h4>
                    @if(! auth()->check())
                        <a href="/login">Sign in</a><br>
                        <a href="/register">Sign up</a><br>
                    @else
                        <a href="/account">My Account</a><br>
                    @endif
                    {{--<a href="#">My Favorites</a></p>--}}
                </div>
                <div class="col-md-2"><h4>Company</h4>
                    <p><a href="/about">About Us</a><br>
                        <a href="/newsletters">Newsletters</a><br>
                        <a href="/contests">Contests</a><br>
                        <a href="/contact">Contact</a><br>
                        <a href="/privacy">Privacy Policy</a></p>
                </div>
                <div class="col-md-4"><h4>Connect</h4>
                    <p>
                        <a href="https://www.facebook.com/pages/App-Treasure-Hunter/490344371027539" target="_blank"><img src="/img/icon-facebook.png" alt=""/></a>
                        <a href="https://twitter.com/AppTreasureHunt" target="_blank"><img src="/img/icon-twitter.png" alt=""/></a>
                        <a href="http://www.pinterest.com/apptreasurehunt/" target="_blank"><img src="/img/icon-pinterest.png" alt=""/></a>
                    </p>
                </div>
            </div>

            <div class="clearfix"></div>
            <p class="copyright">&copy; {{date('Y')}} App Treasure Hunter</p>

        </div></div> <!-- /container -->
</footer>