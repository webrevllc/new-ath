<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>App Treasure Hunter Wishlist</h2>

    <div>
        @foreach($apps as $app)
            <a href="https://apptreasurehunter.com/app/{{$app->url_name}}">{{$app->name}}</a>
            <p>{!! substr($app->description, 0, 100) !!}...</p>
        @endforeach
    </div>

</body>
</html>