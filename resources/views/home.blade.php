@extends('layout')

@section('content')
    <style>
        .col-md-3:nth-child(4n+1){
            clear:left;
        }
    </style>
    <title>App Treasure Hunter</title>

    <div class="slider-wrapper">
        <div id="slider" class='lean-slider'>
            <div class="slide1">
                <div class="masthead leanslide-content">
                    <h1>The Only Site with Over 500 Apps Reviewed by Kids, Parents, & Educators</h1>

                    <h4>"There are a lot of great apps on this site which I have found useful in college prep. I know that I would not have excelled as much as I have without their use. This is a great "one stop shop!" <span>-AR</span></h4>
                    <br><br><br>

                    <div>
                    </div>
                </div>
            </div>
            <div class="slide2">
                <div class="masthead leanslide-content">
                    <h1>Reviews You Can Trust from Parents Who Care. </h1>

                    <h4> There are never any paid reviews or inclusions. Every review is based on a full and careful study of the app.</h4>
                </div></div>
            <div class="slide3">
                <div class="masthead leanslide-content">
                    <h1>Unique Benefits With Your Free Account.  </h1>
                    <h4>Save apps to an emailable wish list. Set up profiles for individual children with information on apps for them. More features to come! </h4>
                </div>
            </div>
        </div>
        <div id="slider-direction-nav"></div>
        <div id="slider-control-nav"></div>
    </div>

    <div class="featured lt-yellow-box">
        <h3>Some of the Best Apps</h3>

        <div class="row">

            @foreach($apps as $app)
                <div class="col-xs-12 col-md-3">
                    <?php $thumb = getThumbs($app->thumb_image_id) ?>
                    <img src="/files/images/versions/small/app_thumbs/{{$thumb->name.'-'.$thumb->id.'.'.$thumb->extension}}" alt="" class="center-block" style="width:auto;"/>
                    <div class="text-center">
                        <div class="app-name app-info-item" style=''>
                            <a href="/app/{{$app->url_name}}">{{substr($app->name, 0, 100)}}
                                {{--@if(strlen($app->name) > 20)--}}
                                {{--...--}}
                                {{--@endif--}}
                            </a>
                        </div>
                        <div class="cat-name app-info-item">
                            <?php $i = 0; $catCount = count($app->categories)?>
                            @foreach($app->categories as $category)
                                <?php if(++$i > 2) {
                                    echo $catCount - 2 .' more';
                                    break;
                                }?>
                                <a href="/category/{{$category->url_name}}">{{$category->name}}</a>,

                            @endforeach
                        </div>
                        <div class="ages app-info-item">
                            Ages:
                            @foreach($app->ageRanges as $ageRange)
                                <a href="/app/agerange/{{$ageRange->url_name}}">{{$ageRange->range}}</a>,
                            @endforeach
                        </div>

                    </div>
                </div><!--end span4-->
            @endforeach

        </div><!--end .row-->
    </div><!--end .featured-->

    <div class="clearfix"></div>
    <div class="row">
        <h2 class="text-center">Who Are We and What is App Treasure Hunter?</h2>
        <p>Our team is a handful of parents and educators with a passion for great education apps.  App Treasure Hunter is the only review site that provides in-depth education app reviews to allow you to make the most informed decision when picking apps for your children.  These apps are reviewed by educational experts and we provide you with the best educational apps online.  Many apps out there are of poor quality, no educational value, some have viruses or the potential for kids to abuse or incur charges.  All the apps we present here are tested for the above points and will allow YOU to make an informed decision to control what and how your kids interact with educational apps.  See what thousands of people are using to find their education apps! Become a subscriber for FREE for a limited time only to discover how our in-depth reviews can transform how you choose and pick educational apps for the sake of your kids.
        </p>
        <div class="text-center"><a href="/register" class="btn">Join Today!</a></div>
    </div>


@endsection

@section('footer')
    <link href="/js/leanslider/lean-slider.css" rel="stylesheet"/>
    <script src="/js/leanslider/lean-slider.js"></script>
    <script>
        $(document).ready(function() {
            $('#slider').leanSlider();
        });
    </script>
@endsection