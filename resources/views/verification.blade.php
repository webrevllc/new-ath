@extends('layout')

@section('content')
<div class="container" ng-controller="verification">
    <div class="alert alert-success" ng-if="sent">
        <h4>Verfication resent - check your email address!</h4>
    </div>

    <div class="alert alert-info" ng-hide="sent">
        <h4>Sorry, you have not verified your email yet. Please check your email for your verification code.</h4>
        <br>
        <button ng-click="resendEmail()" >Resend Verification Code</button>

    </div>
</div>
@endsection

@section('footer')
    <script>
        var id = "{{auth()->user()->id}}";
        app.controller('verification', function($scope, $http){
            $scope.resendEmail = function(){
                var responsePromise = $http.post("/verification", {"id": id});
                responsePromise.success(function (data, status, headers, config) {
                    if(data == 'Email Sent'){
                        $scope.sent=true;
                    }

                });
                responsePromise.error(function (data, status, headers, config) {
                    console.log(data);
                });
            };

        })
    </script>
@endsection