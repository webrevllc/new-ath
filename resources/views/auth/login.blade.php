@extends('layout')

@section('content')
    <div class="container">
        <div class="row col-md-6">
            <h1>Login</h1>
            <div class="panel-body">
                @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <h4 style="margin:0px;">{{ Session::get('error') }}</h4>
                    </div>
                @endif
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    <input type="hidden" value="{{csrf_token()}}" name="_token"/>
                    <label>Email Address:</label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif

                    <br>
                    <label>Password:</label>
                    <input type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                    <br>
                    <div><input type="submit" value="Log In"  class="btn"></div>

                    <br>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                    <p><small><a href="/register">Register</a> | <a  href="{{ url('/password/reset') }}">Forgot Your Password?</a></small></p>
                    <p><a href="/fb"><img src="/img/login-facebook.png" alt=""/></a></p>
                </form>
            </div>
        </div>
    </div>
@endsection
