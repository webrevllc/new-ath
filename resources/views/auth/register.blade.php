@extends('layout')

@section('content')
<div class="container">
    <div class="row">

        <h1>Create an Account</h1>
        <p>Registering for this site is easy, just fill in the fields below and we'll get a new account set up for you in no time.</p>
        <div class="form">
            <form id="register-form" role="form" method="POST" action="{{ url('/register') }}" class="form-horizontal">
                {{csrf_field()}}
                <div class="row-fluid">
                    <div class="col-md-6 pull-left">
                        <h3>Account Details</h3>
                        @if (count($errors) > 0)
                            <div class="alert alert-error">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="control-group {{ $errors->has('email') ? ' error' : '' }}">
                            <label for="UserRegisterForm_username" class="required">E-mail Address <span class="required">*</span></label>
                            <input name="email" value="{{ old('email') }}" id="UserRegisterForm_username" type="email" maxlength="255" required class="{{ $errors->has('email') ? ' has-error' : '' }} form-control" />
                        </div>

                        <div class="control-group {{ $errors->has('password') ? ' error' : '' }}">
                            <label for="UserRegisterForm_password" class="required">Password: <span class="required">*</span></label>
                            <input name="password" value="{{ old('password') }}" id="UserRegisterForm_password" type="password" maxlength="255" required class="form-control"/>
                        </div>

                        <label for="UserRegisterForm_password_repeat" class="required">Confirm Password: <span class="required">*</span></label>
                        <input name="password_confirmation"  id="UserRegisterForm_password_repeat" type="password" maxlength="255" required class="form-control"/>

                        <h3>Profile Details</h3>
                        <label for="UserProfile_first_name">First Name: <em>(Not Required)</em></label>
                        <input name="UserProfile[first_name]" id="UserProfile_first_name" type="text" maxlength="255" class="form-control"/>

                        <label for="UserProfile_last_name">Last Name: <em>(Not Required)</em></label>
                        <input name="UserProfile[last_name]" id="UserProfile_last_name" type="text" maxlength="255" class="form-control"/>
                        <br>
                        <p class="text-center"><input type="submit" value="Complete Sign Up" class="btn pull-right"></p>
                    </div><!--end .span6-->
                    <div class="col-md-6 pull-left">
                        <h3>Or</h3>
                        <p><a href="/fb"><img src="/img/facebook_signup_button.png" alt=""/></a></p>
                    </div>
                </div><!--end row-->


            </form>
        </div><!-- form -->
    </div>
</div>
@endsection
