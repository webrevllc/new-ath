@extends('layout')
@section('content')

    <div class="row reviews">
        <div class="col-md-3 lt-yellow-box left-column">
            <div class="thumb" ng-controller="student">
                <?php
                    if($theApp->thumb_image_id){
                        $thumb = getThumbs($theApp->thumb_image_id);
                    }
                ?>
                @if($theApp->thumb_image_id)
                        <img src="/files/images/versions/small/app_thumbs/{{$thumb->name.'-'.$thumb->id.'.'.$thumb->extension}}" alt="" width="75%" class="center-block"/>
                @endif
                    <br>
                    <br>
                    @if(auth()->check())
                        @if(count(auth()->user()->students) > 0)

                            <select ng-options="student.first_name for student in students track by student.id"
                                    ng-model="selectedstudent"
                                    ng-change="checkApp({{$theApp->id}}, selectedstudent.id )">
                                <option value="">Select Child</option>
                            </select>
                            {{--<select>--}}
                                {{--@foreach(auth()->user()->students as $student)--}}
                                    {{--<option value="{{$student->id}}">{{$student->first_name}} {{$student->last_name}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            <div  ng-show="ownsApp == 1">
                                <a href="/account/students/addApp/{{$theApp->id}}/<% selectedstudent.id %>">Add to Child's Profile</a>
                            </div>
                            <div ng-show="ownsApp == 2">
                                <a href="/account/students/removeApp/{{$theApp->id}}/<% selectedstudent.id %>" >Remove from Child's Profile</a>
                            </div>
                            <div><a href="/account/wishlist/{{$theApp->id}}">Save to Wish List</a></div>
                        @else()
                            <div>You do not have any students yet, <a href="{{'/account/students/create'}}">add a student.</a></div>
                        @endif
                    @else
                        <div><a href="/login">Login</a> or <a href="/register">Register</a> to Create a wishlist.</div>
                    @endif

            </div>

            <div class="details">
                <div class="price">${{$theApp->price}} at time of review</div>
                {{--<div class="compatibility">[Devices Available]</div>--}}

                <div class="links">
                    <div class="itunes"><a href="{{$theApp->itunes_url}}" target="_blank"><img src="/img/icon-itunes-small.png" alt=""/> Download from iTunes</a></div>
                    @if($theApp->youtube_url != '')
                        <div class="video-review">
                            <h3>YouTube Trailer:</h3>
                            <div>
                                <a href="{{$theApp->youtube_url}}" data-lity><img src="/img/icon-youtube-small.png" alt=""/> Watch The Trailer!</a>
                                {{--<iframe width="100%" height="100px" src="" frameborder="0" allowfullscreen></iframe>--}}
                            </div>
                        </div>

                    @endif
                    @if(count($theApp->reviewer) > 0)
                        <div class="video-review">
                            <h3>Video Review:</h3>
                            <div>
                                <a href="{{$theApp->reviewer->url}}" data-lity><img src="/img/icon-youtube-small.png" alt=""/> Watch The Review!</a>
                                {{--<iframe width="100%" height="100px" src="" frameborder="0" allowfullscreen></iframe>--}}
                            </div>
                        </div>
                    @endif
                </div>




            </div><!--end .details-->
        </div><!--end .span2-->

        <div class="col-md-9">
            @if(Session::has('success'))
                <div class="alert alert-success span9">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error span9">
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="review">
                <div class="title"><h1>{{$theApp->name}}</h1>
                    @if(auth()->check())
                        @can('manage_site')
                            <div>
                                Admin Options:
                                <a href="{{route('admin.apps.cancelApproval', [$theApp->id])}}">[Cancel Approval]</a>
                                <a href="{{route('admin.apps.edit', [$theApp->id])}}">[Edit]</a>
                                <a href="{{route('admin.apps.deleteApp', [$theApp->id])}}">[Delete]</a>
                                <a href="{{route('admin.apps.testimonial', [$theApp->id])}}">[Manage Testimonials]</a>
                                <a href="{{route('admin.apps.screenshots', [$theApp->id])}}">[Manage Screenshots]</a>
                            </div>
                        @endcan
                    @endif
                    <div class="specs">In
                        @foreach($theApp->subcategories as $subcategory)
                            <a href="/subcategory/{{$subcategory->url_name}}">{{$subcategory->name}}</a>,
                        @endforeach

                        @foreach($theApp->categories as $category)
                            <a href="/category/{{$category->url_name}}">{{$category->name}}</a>,
                        @endforeach
                    </div>

                    <div class="specs">Age Level:
                        @foreach($theApp->ageRanges as $ageRange)
                            <a href="/app/agerange/{{$ageRange->url_name}}">{{$ageRange->range}}</a>,
                        @endforeach
                    </div>
                    {{--<div class="awards">[Accolades if Any]</div>--}}
                    <div class="developer">By Developer: <a href="/developer/{{$theApp->developer->url_name}}">{{$theApp->developer->name}}</a></div>
                    <hr>
                    <div><p>{!! $theApp->description !!}</p></div>
                    <div><h2>Features</h2>
                        {!! $theApp->features !!}
                    </div>
                    @if(count($theApp->reviewer) > 0)
                        <h2>Video Review</h2>
                        <a href="{{$theApp->reviewer->url}}" data-lity><img src="/img/icon-youtube-small.png" alt=""/> Watch the review!</a>
                    @endif
                    @if($theApp->youtube_url != '')
                        <h2>YouTube Trailer</h2>
                        <a href="{{$theApp->youtube_url}}" data-lity><img src="/img/icon-youtube-small.png" alt=""/> Watch The Trailer!</a>
                    @endif
                    @if(count($theApp->screenshots) > 0)
                        <div class="screen-shots">
                            <h2>Screen Shots</h2>
                            <div>
                                @foreach($theApp->screenshots as $screenshot)
                                    <?php $thumb = getThumbs($screenshot->id) ?>
                                    <img src="/files/images/versions/screenshot/app_screenshots/{{$thumb->name.'-'.$thumb->id.'.'.$thumb->extension}}" alt="" />
                                @endforeach
                            </div>
                        </div>
                    @endif

                    {{--<div class="testimonial">--}}
                        {{--<h3>Testimonials</h3>--}}
                        {{--<blockquote>--}}
                            {{--<p>[Testimonial]</p>--}}
                            {{--<p class="credit">-[Testimonial credit]</p>--}}
                        {{--</blockquote>--}}
                    {{--</div>--}}

                </div><!--end review-->

                {{--<div class="buttons text-center"><a href="#" class="btn btn-small">next simpler</a> <a href="#"  class="btn  btn-small">next more complex</a></div>--}}


            </div><!--end .span9-->

        </div><!--end .row-->
    </div>




@stop

@section('footer')
    <script>
        app.controller('student', function($scope, $http){

            $scope.selectedStudent = '';
            var responsePromise = $http.get("/students/");
            responsePromise.success(function(data, status, headers, config) {
                $scope.students = data.students;
            });
            responsePromise.error(function(data, status, headers, config) {
                console.log(data);
            });

            $scope.checkApp = function(id, studentId){

                var responsePromise = $http.get("/account/studentapp/" + id + '/' + studentId);
                responsePromise.success(function(data, status, headers, config) {
                    console.log(data);
                    if(data == "true"){
                        $scope.ownsApp = 2;
                    }else{
                        $scope.ownsApp = 1;
                    }
                    $scope.studentSelected = true;

                });
                responsePromise.error(function(data, status, headers, config) {
                });
            }
        });
    </script>
@endsection