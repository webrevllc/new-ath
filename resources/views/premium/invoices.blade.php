@extends('layout')
@section('content')
<h2>Invoices</h2>
<table class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>Date</th>
            <th>Amount</th>
            <th>Download</th>
        </tr>
    </thead>
    @foreach ($invoices as $invoice)
        <tr>
            <td>{{ $invoice->date()->toFormattedDateString() }}</td>
            <td>{{ $invoice->total() }}</td>
            <td><a href="/user/invoice/{{ $invoice->id }}">Download</a></td>
        </tr>
    @endforeach
</table>
@endsection