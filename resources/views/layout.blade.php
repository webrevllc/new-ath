<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <title>App Treasure Hunter</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="author" content="SD Rosenthal - WebRev LLC">

    <!-- Le styles -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/fonts.css" rel="stylesheet">
    <link href="/css/lity.min.css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    @yield('header')
</head>

<body>

@include('nav')

<div id="Content">
    <div class="container">
        @yield('content')
    </div><!-- /.container-->
</div><!-- /#Content-->

@include('footer')

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
<script>
    var cat = "{{Request::has('category')}}" ? "{{Request::get('category')}}" : 0;
    var sub = "{{Request::has('subcategory')}}" ? "{{Request::get('subcategory')}}" : 0;
    var words = "{{Request::has('term')}}"? "{{Request::get('term')}}" : '' ;
    var ages = "{{Request::has('range')}}" ? "{{Request::get('range')}}" : 0;

    var app = angular.module('app', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

    app.controller('categoryCtrl', function($scope, $http){

        $scope.form = {};
        $scope.form.words = words;



        $scope.getAgeRanges = function(){
            var responsePromise = $http.get("/ageranges/");
            responsePromise.success(function(data, status, headers, config) {
                $scope.ageranges = data.ages;
                $scope.form.selectedAgerange = data.ages[getIndexOf($scope.ageranges, ages, 'id')];

            });
            responsePromise.error(function(data, status, headers, config) {
                console.log(data);
            });
        };

        $scope.getCategories = function(){
            var responsePromise = $http.get("/categories/");
            responsePromise.success(function(data, status, headers, config) {
                $scope.categories = data.categories;
                $scope.form.selectedCategory = data.categories[getIndexOf($scope.categories, cat, 'id')];
//                console.log($scope.categories);
                $scope.getSubs();
            });
            responsePromise.error(function(data, status, headers, config) {
                console.log(data);
            });
        };

        $scope.getSubs = function(){
            var responsePromise = $http.get("/subcategories/" + $scope.form.selectedCategory.id);
            responsePromise.success(function(data, status, headers, config) {
                $scope.subcategories = data.subcategories;
                $scope.form.selectedSubcategory = data.subcategories[getIndexOf($scope.subcategories, sub, 'id')];
//                console.log($scope.subcategories);
            });
            responsePromise.error(function(data, status, headers, config) {
                console.log(data);
            });
        };

        $scope.getCategories();
        $scope.getAgeRanges();
        $scope.doSomething = function(){
            window.location = ($('#searchButton').attr('href'));
        };

        function getIndexOf(arr, val, prop) {
            var l = arr.length,
                    k = 0;
            for (k = 0; k < l; k = k + 1) {
                if (arr[k][prop] == val) {
                    return k;
                }
            }
            return false;
        }
    });

    app.directive('myEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.myEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });
</script>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<script src="/js/lity.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@yield('footer')

</body>
</html>
