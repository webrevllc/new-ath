@extends('layout')
@section('content')
    <div  ng-controller="resultsController">
        <div class="pagination pull-right">{!! $apps->appends(Request::except('page'))->links() !!}</div>
        <h1 style="margin:0px;">{{count($apps)}} Search Results </h1>
        <h4># Results Per Page</h4>
        <div class="col-md-2">
            {{Form::select('results',
                [],
                null,
                [
                'ng-model' => 'selected',
                'ng-options' => 'size.id as size.label for size in sizes ',
                'ng-change' => "getSome()",
                'class' => 'form-control'
                ]
            )}}
        </div>
        <div class="clearfix"></div>


        <div class="search-results">

            @if(count($apps) < 1)
                  <h4>Sorry, no apps meet that criteria.</h4>
            @endif
            @foreach($apps as $k => $app)
                <!--For each odd result-->
                <div class="result
                    {{--@if($k&1)--}}
                        {{--search-result-alt--}}
                    {{--@endif--}}
                ">
                    <div class="thumb">
                        <?php $thumb = getThumbs($app->thumb_image_id) ?>
                        <a href="/app/{{$app->url_name}}">
                            @if($thumb)
                                <img src="/files/images/versions/small/app_thumbs/{{$thumb->name.'-'.$thumb->id.'.'.$thumb->extension}}" alt="" />
                            @endif
                        </a>
                    </div>

                    <div class="text">
                        <div class="app-name">
                            <a href="/app/{{$app->url_name}}">{{$app->name}}</a>
                        </div>
                        <div class="app-specs">
                            In
                            @foreach($app->categories as $category)
                                <a href="/category/{{$category->url_name}}">{{$category->name}}</a>
                            @endforeach
                        </div>
                        <div>
                            Age Level:
                            @foreach($app->ageRanges as $ageRange)
                                <a href="/app/agerange/{{$ageRange->url_name}}">{{$ageRange->range}}</a>
                            @endforeach
                        </div>
                        <div>Price: ${{$app->price}}</div>
                        <div>{!! str_limit($app->description, 200) !!}</div>
                    </div>
                    <div class="clearfix"></div>
                </div><!--end .result --><!--end each odd result-->
                <div class="clearfix"></div>
            <hr>
            @endforeach
                <div class="clearfix"></div>
        </div><!--end .search-results-->
        <div class="pagination">{!! $apps->appends(Request::except('page'))->links() !!}</div>
    </div>

@stop

@section('footer')
    <script>
        var amount = "{{Request::has('results') ? Request::get('results') : 25}}";
        app.controller('resultsController', function($scope, $http){
            $scope.selected = amount;
            $scope.getSome = function(){
                window.location.href = "/search" + window.location.search + "&results=" + $scope.selected;
            };
            $scope.sizes = [{
                id: '10',
                label: '10',
                value: 10
            }, {
                id: '25',
                label: '25',
                value: 25
            }, {
                id: 'all',
                label: 'All',
                value: 9999
            }];
        });
    </script>
@endsection