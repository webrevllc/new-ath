{{--<header>--}}
    {{--<div class="navbar navbar-inverse navbar-fixed-top">--}}
        {{--<div class="navbar-inner">--}}
            {{--<div class="container">--}}
                {{--<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">--}}
                    {{--<span class="icon-bar"></span>--}}
                    {{--<span class="icon-bar"></span>--}}
                    {{--<span class="icon-bar"></span>--}}
                {{--</button>--}}

                {{--<div class="tag">The Only Site with Over 500 Apps Reviewed by Kids, Parents, &amp; Educators</div>--}}

                {{--<div class="logo "><a href="/"><img src="/img/logo.png" alt="App Treasure Hunter"/></a></div>--}}
                {{--<a class="brand visible-phone visible-tablet " href="/"><img src="/img/logo.png" alt="App Treasure Hunter"/></a>--}}

                {{--<div class="account">--}}
                    {{--<div>JOIN TODAY!</div>--}}
                    {{--@if(! auth()->check())--}}
                        {{--<a href="/login">Log In</a> or <a href="/register">Sign Up</a><!--End account-->--}}
                    {{--@else--}}
                        {{--<a href="/account">{{auth()->user()->username}}</a> | <a href="/logout">logout</a>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<ul class="nav navbar-nav navbar-right">--}}
                    {{--<li><a href="#">Link</a></li>--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                            {{--<li role="separator" class="divider"></li>--}}
                            {{--<li><a href="#">Separated link</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                {{--<div class="nav-collapse">--}}
                    {{--<div class="navedge1"></div>--}}
                    {{--<div class="navedge2"></div>--}}
                    {{--<ul class="nav navbar-right">--}}
                        {{--<li><a href="/featured-apps">Featured Apps</a></li>--}}
                        {{--<li class="dropdown">--}}
                            {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">Ask Lyn</a>--}}
                            {{--<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">--}}
                                {{--<li><a href="/ask-lyn">Ask Lyn</a></li>--}}
                                {{--<li><a href="/what-is-ask-lyn">What is Ask Lyn</a></li>--}}
                                {{--<li><a href="/newsletters">Newsletters</a></li>--}}
                                {{--<li><a href="/curriculum">Curriculum</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="/new-apps">What's New</a></li>--}}
                        {{--<li><a href="/about">About Us</a></li>--}}
                        {{--<li class="dropdown">--}}
                            {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us</a>--}}
                            {{--<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">--}}
                                {{--<li><a href="/about">The Company</a></li>--}}
                                {{--<li><a href="/contests">Contests</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}

                        {{--<li><a href="/contact">Contact</a></li>--}}
                    {{--</ul>--}}

                    {{--<div class="nav2" ng-controller="categoryCtrl">--}}
                        {{--<label>Search Apps</label>--}}
                        {{--{{Form::select('crit[range]',--}}
                            {{--[],--}}
                            {{--null,--}}
                            {{--[--}}
                            {{--'ng-model' => 'form.selectedAgerange',--}}
                            {{--'ng-options' => 'agerange.range for agerange in ageranges track by agerange.id',--}}
                            {{--'placeholder' => 'Age Range'--}}
                            {{--]--}}
                            {{--)}}--}}

                        {{--{{Form::select('crit[category]',--}}
                            {{--[],--}}
                            {{--null,--}}
                            {{--[--}}
                            {{--'ng-model' => 'form.selectedCategory',--}}
                            {{--'ng-options' => 'category.name for category in categories track by category.id',--}}
                            {{--'ng-change' => 'getSubs()',--}}
                            {{--'placeholder' => 'Category'--}}
                            {{--]--}}
                            {{--)}}--}}

                        {{--{{Form::select('crit[sub]',--}}
                            {{--[],--}}
                            {{--null,--}}
                            {{--[--}}
                            {{--'ng-model' => 'form.selectedSubcategory',--}}
                            {{--'ng-options' => 'subcategory.name for subcategory in subcategories track by subcategory.id',--}}
                            {{--'placeholder' => 'Subcategory'--}}
                            {{--]--}}
                            {{--)}}--}}

                        {{--<input type="text" id="search" class="search" placeholder="Search Keyword(s)" ng-model="form.words" name="crit[words]">--}}
                        {{--<a href="/search?range=<% form.selectedAgerange.id %>&category=<% form.selectedCategory.id %>&subcategory=<% form.selectedSubcategory.id %>&term=<% form.words %>">--}}
                            {{--<img src="/img/search.png" class="submit" />--}}
                        {{--</a>--}}
                    {{--</div><!--/.nav2-->--}}
                {{--</div><!--/.nav-collapse -->--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</header>--}}

<header >
    <nav class="navbar navbar-inverse navbar-fixed-top ">
        <div class="navbar-inner">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed btn btn-navbar" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="tag">The Only Site with Over 500 Apps Reviewed by Kids, Parents, &amp; Educators</div>

                    <div class="logo brand"><a href="/"><img src="/img/logo.png" alt="App Treasure Hunter"/></a></div>
                    {{--<a class="brand" href="/"><img src="/img/logo.png" alt="App Treasure Hunter"/></a>--}}
                </div>



                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="position: relative;">
                    <div class="navedge1"></div>
                    <div class="navedge2"></div>
                    <ul class="nav navbar-nav navbar-right" >

                        <li><a href="/featured-apps">Featured Apps</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Ask Lyn</a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li><a href="/ask-lyn">Ask Lyn</a></li>
                                <li><a href="/what-is-ask-lyn">What is Ask Lyn</a></li>
                                <li><a href="/newsletters">Newsletters</a></li>
                                <li><a href="/curriculum">Curriculum</a></li>
                            </ul>
                        </li>
                        <li><a href="/new-apps">What's New</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us</a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li><a href="/about">The Company</a></li>
                                <li><a href="/contests">Contests</a></li>
                            </ul>
                        </li>

                        <li><a href="/contact">Contact</a></li>
                    </ul>
                    <div class="nav2" ng-controller="categoryCtrl" my-enter="doSomething()">
                        <label>Search Apps</label>
                        {{Form::select('crit[range]',
                        [],
                        null,
                        [
                        'ng-model' => 'form.selectedAgerange',
                        'ng-options' => 'agerange.range for agerange in ageranges track by agerange.id',
                        'placeholder' => 'Age Range'
                        ]
                        )}}

                        {{Form::select('crit[category]',
                        [],
                        null,
                        [
                        'ng-model' => 'form.selectedCategory',
                        'ng-options' => 'category.name for category in categories track by category.id',
                        'ng-change' => 'getSubs()',
                        'placeholder' => 'Category'
                        ]
                        )}}

                        {{Form::select('crit[sub]',
                        [],
                        null,
                        [
                        'ng-model' => 'form.selectedSubcategory',
                        'ng-options' => 'subcategory.name for subcategory in subcategories track by subcategory.id',
                        'placeholder' => 'Subcategory'
                        ]
                        )}}

                        <input type="text" id="search" class="search" placeholder="Search Keyword(s)" ng-model="form.words" name="crit[words]">
                        <a id="searchButton" href="/search?range=<% form.selectedAgerange.id %>&category=<% form.selectedCategory.id %>&subcategory=<% form.selectedSubcategory.id %>&term=<% form.words %>">
                            <img src="/img/search.png" class="submit" />
                        </a>
                    </div><!--/.nav2-->
                </div><!-- /.navbar-collapse -->

                <div class="account" style="z-index: 1000; margin-top:-25px">
                    <div>JOIN TODAY!</div>
                    @if(! auth()->check())
                        <a href="/login">Log In</a> or <a href="/register">Sign Up</a><!--End account-->
                    @else
                        <a href="/account">{{auth()->user()->username}}</a> | <a href="/logout">logout</a>
                    @endif
                </div>
            </div><!-- /.container-fluid -->
        </div>

    </nav>
</header>
