@extends('layout')
@section('content')

    <title>About App Treasure Hunter</title>
    <h1>Our Mission</h1>
    <p>App Treasure Hunter&rsquo;s team is a group of dedicated individuals, all sharing the common interest of creating a positive impact on the lives of others.  Our way of doing this?  By backing up the educational system using &ldquo;the best of the best&rdquo; educational apps.</p>
    <p>There is nothing like seeing a child, or for that matter, an adult, brighten up and get excited about learning.  We at App Treasure Hunter have seen this countless times – such as when the  weight of a confusion is lifted from a child&rsquo;s shoulders, supplanted by an understood concept.  Or, from the result of a more thoroughly understood subject.  And what does this result in? Competency.  Competency is a balance of understanding of the &ldquo;theory&rdquo; of a subject, being able to apply it and practicing until one masters it.  We know that competence is the key to achieving educational based goals and much more.  Society is littered with educational failures and built from it&rsquo;s successes.</p>
    <p>We invite you to partake in our website and reviews.  We hope you will greatly benefit from our endeavours.<strong></strong></p>
    <h2>What Makes Us Different?</h2>
    <p>Did you know that an average of 500 new apps, for the iPhone and iPad, are released every single day? We have found that a great deal of them are not of the best quality. However, there are numerous apps that are of high quality, very educational and fun!</p>
    <p>If you are like us, you want to find those educational gems too and have your children benefit from them. Let&rsquo;s face it, downloading a low quality app will not only be a waste of your time and money, but will be left to grow static, untouched by your children.  Since kids like to play games, we have found numerous apps that are very educational as well as providing game-like fun.</p>
    <p>This is where the idea of App Treasure Hunter came to be.  We had the idea of providing the service of culling the App Store for you, saving you time, frustration and possibly money.  Plus helping to improve the educational level of the future generation. Become a full subscriber <strong><a title="Register" href="http://www.apptreasurehunter.com/register/">click here</a></strong> to gain access to hundreds of professionally reviewed and tested apps. Or, <a href="http://www.apptreasurehunter.com/blog/newsletters/"><strong>click here</strong></a><strong> </strong> to only receive our free weekly newsletter.</p>
    <h3>A Bit About Us</h3>
    <p><img alt="About Us - Bob-Twaalfhoven" width="150" height="150" src="/img/staff/Bob-Twaalfhoven-150x150.jpg" class="img-left"><strong>Bob Twaalfoven</strong> is the envisionary of App Treasure Hunter, as well as a graduate from MIT.  As a  father of 2 very bright boys, Bob has a strong interest in education and ensuring his children grow to be competent adults.  They are well on their way in doing just that.  Bob knows that a good education is key, not only to successful children but a successful civilization as well.</p>

    <div class="clearfix"></div><hr>

    <p><img alt="Teri-Rigdon" width="150" height="150" src="/img/staff/Teri-Rigdon-150x150.jpg" class="img-left"><strong>Teri Rigdon</strong> is the Executive Administrator.  She has written many of the reviews and has a staff of writers under her.  She also writes the weekly newsletter.  Teri, having been an educator herself, she loves the concept of App Treasure Hunter and the purpose behind creating it.  When Bob came up with the idea of App Treasure Hunter she took to it like a &ldquo;duck in water&rdquo;!</p>

    <div class="clearfix"></div><hr>

    <p><img alt="Lynn" width="150" height="150" src="/img/staff/Lynn-150x150.jpg" class="img-left"><br>
        <strong>Lyn Demaree</strong> has been involved in the field of education for over 40 years. She has 4 children and thousands of students of all levels who are successfully using their learning capacity in life. She majored in Neurobiology and Linguistics at UC Santa Cruz and has lived &amp; taught all over the world.</p>

    <div class="clearfix"></div><hr>

    <p><strong><img alt="Collette-Matthews" width="150" height="150" src="/img/staff/Collette-Matthews1-150x150.jpg" class="img-left">Colette Matthews</strong> is one of our key players. She has taken on the roll of Marketing Director with enthusiasm and skill.  Colette has a strong educational background, and is a key player in our cause.  She is making a difference in reaching out to others who share our common goal.<strong></strong></p>

    <div class="clearfix"></div><hr>

    <p><img alt="Charles Verhoeff" width="150" height="150" src="/img/staff/Charles-150x150.jpg" class="img-left"></p>
    <p><strong>Charles Verhoeff</strong> is our Webmaster and a great father. He has two kids of his own and is proud to forward a purpose such as our own.</p>

@stop