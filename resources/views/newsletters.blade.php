@extends('layout')

@section('content')
    @foreach($newsletters as $n)
        <li><a href="/newsletters/{{$n->id}}">{{$n->title}}</a></li>
    @endforeach
@endsection