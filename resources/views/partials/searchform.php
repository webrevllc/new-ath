

    <label>Search Apps</label>
    <select ng-options="agerange.range for agerange in ageranges track by agerange.id" ng-model="form.selectedAgerange" name="crit[range]">
        <option value="">Age Range</option>
    </select>


    <select ng-options="category.name for category in categories track by category.id" ng-model="form.selectedCategory" ng-change="getSubs()" name="crit[category]">
        <option value="">Category</option>
    </select>

    <select ng-options="subcategory.name for subcategory in subcategories track by subcategory.id" ng-model="form.selectedSubcategory" name="crit[sub]">
        <option value="">Subcategory</option>
    </select>

    <input type="text" id="search" class="search" placeholder="Search Keyword(s)" ng-model="words" name="crit[words]">
    <input type="image" name="imageField" id="imageField" src="/img/search.png" class="submit">

