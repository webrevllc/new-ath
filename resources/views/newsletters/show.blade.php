@extends('layout')

@section('content')
   <h3>{{$newsletter->title}}</h3>
   {{$newsletter->body}}
@endsection