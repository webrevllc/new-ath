@extends('layout')

@section('content')

<title>My Account |App Treasure Hunter</title>

<div class="row reviews">
    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/users">Users</a> &raquo; {{$user->profile->first_name.' '. $user->profile->last_name}}
    <div class="span12">
        {{--<a href="{{route('admin.users.edit', [$user->id])}}">[Edit]</a>--}}
        <h1>{{$user->profile->first_name.' '. $user->profile->last_name}}</h1>
        <p>Age: {{getUserAge($user->profile)}}</p>
        <p>Country: {{$user->profile->country}}</p>
        <p>Gender: {{$user->profile->gender}}</p>
        <p>Interested for: {{$user->profile->interested_for}}</p>

        <h3>Students</h3>
        @if(count($user->students) > 0)
            @foreach($user->students as $student)
                <h4><a href="/student/3">{{$student->first_name.' '. $student->last_name}}</a></h4>
                <div>Age: {{getStudentAge($student)}}</div>
                <div>Learning Level: {{$student->learning_level}}</div>
                <div>Apps owned:</div>
                <div>Subjects of interest: {{$student->subjects_of_interest}}</div>
                <div>Comments: {{$student->comments}}</div>
            @endforeach
        @else
            <a href="#">+ Add New Student</a>
        @endif
    </div>
    <!--end .span9-->

</div><!--end .row-->
@endsection