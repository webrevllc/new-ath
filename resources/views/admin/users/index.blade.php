@extends('layout')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h4>{{ Session::get('success') }}</h4>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <h4>{{ Session::get('error') }}</h4>
        </div>
    @endif
    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; Users
    <h1>Manage Users</h1>
    {{--<a class="admin_btn" href="#">Add New</a>--}}
    <div class="container">
        @if(Session::has('success'))
            <div class="alert alert-success span9">
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-error span9">
                {{Session::get('error')}}
            </div>
        @endif
        <div class="clearfix"></div>
        <table class="admin-manage-table table table-bordered">
            <tr>
                <th>Username (Email)</th>
                <th>Name</th>
                <th>Date Joined</th>
                <th>Roles</th>
                <th>Verified</th>
                <th>Deleted?</th>
                <th>Actions</th>
            </tr>
            @foreach($users as $user)
                <tr class="text-center">
                    <td class="text-left"><a href="{{route('users.show', [$user->id])}}">{{$user->username}}</a></td>
                    <td class="text-left">{{$user->profile->first_name}} {{$user->profile->last_name}}</td>
                    <td class="text-left">{{date('F j, Y', strtotime($user->created_at))}}</td>
                    <td>
                        @foreach($user->roles as $role)
                            {{$role->label}}
                        @endforeach
                    </td>
                    <td>{{$user->is_verified == 1 ? 'Verified' : 'No'}}</td>
                    <td>{{$user->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                    <td><a href="{{route('users.edit', [$user->id])}}">Edit</a>
                        {{--{{$user->is_deleted}}--}}
                        @if($user->is_deleted == 0)
                            | <a href="#deleteUser" data-toggle="modal" data-userid="{{$user->id}}" data-useremail="{{$user->email}}">Delete User</a>
                        @endif
{{--                        <a href="{{route('admin.users.deleteUser', [$user->id])}}">{{$user->is_deleted == 1 ? 'Un-delete' : 'Delete'}}</a>--}}

                    </td>
                    {{--<td class="text-left">{{$user->roles}}</td>--}}
                </tr>
                <div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                Are you sure you want to delete this user?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                                <a class="btn btn-danger" href="{{route('admin.users.deleteUser', [$user->id])}}">Yes</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </table>
    </div>



@endsection

@section('footer')
    <script>
        $('#deleteUser').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var userid = button.data('userid'); // Extract info from data-* attributes
            var useremail = button.data('useremail'); // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find('.modal-title').text('Delete ' + useremail + '?');
//            modal.find('.modal-body input').val(recipient);
        })
    </script>
@endsection