@extends('layout')

@section('content')

    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/users">Users</a> &raquo; Edit
    <h1>Edit {{$user->profile->first_name.' '.$user->profile->last_name}}</h1>

    <div class="container">

        <div class="form">
            <form id="developer-edit-form" action="{{route('admin.users.update', [$user->id])}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PUT">
                @if(Session::has('success'))
                    <div class="alert alert-success span9">
                        {{Session::get('success')}}
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-error span9">
                        {{Session::get('error')}}
                    </div>
                @endif
                <div class="clearfix"></div>
                <div class="row-fluid">

                    <div class="form-group">
                        <label for="User_username">Username</label>
                        <input name="User[username]" id="User_username" type="text" maxlength="255" value="{{$user->username}}" class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label for="User_is_deleted">Is Deleted</label>
                        <input name="User[is_deleted]" id="User_is_deleted" type="text" value="{{$user->is_deleted}}" class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label>First Name</label>
                        <input name="UserProfile[first_name]" id="UserProfile_first_name" type="text" maxlength="255" value="{{$user->profile->first_name}}" class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label>Last Name</label>
                        <input name="UserProfile[last_name]" id="UserProfile_last_name" type="text" maxlength="255" value="{{$user->profile->last_name}}" class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label>User Role
                            @if(count($user->roles) > 0)
                                - Currently:
                                @foreach($user->roles as $role)
                                    {{$role->label}}
                                @endforeach
                            @endif
                        </label>
                        <select name="UserRole[role_id]" id="UserRole_role_id" class="form-control">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->label}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group">
                        <input type="submit" class="btn pull-right">
                    </div>

                </div><!--end row-->


            </form>
        </div><!-- form -->
    </div>
@endsection