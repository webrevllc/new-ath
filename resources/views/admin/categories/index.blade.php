@extends('layout')

@section('content')

    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; Categories
    <div class="clearfix"></div>
    @if(Session::has('success'))
        <div class="alert alert-success col-md-12">
            {{Session::get('success')}}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-error col-md-12">
            {{Session::get('error')}}
        </div>
    @endif
    <div class="clearfix"></div>
    <h1>Manage Categories</h1>
    <a class="admin_btn" href="{{route('categories.create')}}">Add New</a>
    <div class="container">

        <div class="clearfix"></div>
        <table class="table table-bordered table-condensed">
            <tr>
                <th>Category</th>
                <th>URL</th>
                {{--<th>Deleted</th>--}}
                <th>Actions</th>
            </tr>
            @foreach($categories as $category)
                <tr class="text-center">
                    <td class="text-left"><a href="/category/{{$category->url_name}}">{{substr($category->name, 0, 80)}}</a></td>
                    <td class="text-left">{{$category->url_name}}</td>
                    {{--<td>{{$category->is_deleted == 1 ? 'Deleted' : 'No'}}</td>--}}
                    <td class="text-left">
                        <a href="{{route('categories.edit', [$category->id])}}">Edit</a> |
                        <a href="{{route('admin.categories.deleteCategory', [$category->id])}}">Delete</a> |
                        <a href="{{route('admin.categories.addSub', [$category->id])}}">Add Subcategory</a>
                    </td>
                </tr>
                @foreach($category->subcategories as $subcategory)
                    @if($subcategory->is_deleted != true)
                        <tr class="text-center">
                            <td class="text-left"><a href="/subcategory/{{$subcategory->url_name}}">--{{substr($subcategory->name, 0, 80)}}</a></td>
                            <td class="text-left">{{$subcategory->url_name}}</td>
                            {{--<td>{{$subcategory->is_deleted == 1 ? 'Deleted' : 'No'}}</td>--}}
                            <td class="text-left">
                                <a href="{{route('subcategories.edit', [$subcategory->id])}}">Edit</a> |
                                <a href="{{route('admin.subcategories.deleteSubcategory', [$subcategory->id])}}">Delete</a></td>
                        </tr>
                    @endif
                @endforeach
            @endforeach
        </table>
    </div>

@endsection