@extends('layout')

@section('content')

    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/levels">Levels</a> &raquo; Edit
    <h1>Manage {{$level->level}}</h1>
    <div class="container">
        <div class="form">
            @if(Session::has('success'))
                <div class="alert alert-success span9">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error span9">
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="clearfix"></div>
            <form id="developer-edit-form" action="{{route('admin.learninglevels.update', [$level->id])}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PUT">
                <div class="row">
                    <p class="note">Fields with <span class="required">*</span> are required.</p>
                </div>

                <div class="form-group">
                    <label for="Developer_name" class="required">Name <span class="required">*</span></label>
                    <input name="Level[level]" id="Developer_name" type="text" maxlength="255" value="{{$level->level}}" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="Developer_url_name">Url Name</label>
                    <input name="Level[url_name]" id="Developer_url_name" type="text" maxlength="255" value="{{$level->url_name}}" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="Developer_is_deleted">Is Deleted</label>
                    <input name="Level[is_deleted]" id="Developer_is_deleted" type="text" value="{{$level->is_deleted}}" class="form-control"/>
                </div>

                <div class="row buttons">
                    <input type="submit" name="yt0" value="Submit" class="btn pull-right"/>	</div>

            </form>
        </div><!-- form -->

    </div>
@endsection