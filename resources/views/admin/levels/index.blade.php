@extends('layout')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success span9">
            {{Session::get('success')}}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-error span9">
            {{Session::get('error')}}
        </div>
    @endif
    <div class="clearfix"></div>
    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; Learning Levels
    <h1>Manage Learning Levels</h1>
    <a class="admin_btn" href="{{route('admin.learninglevels.create')}}">Add New</a>
    <div class="container">

        <div class="clearfix"></div>
        <table class="table table-bordered table-condensed">
            <tr>
                <th>Developer</th>
                <th>Deleted</th>
                <th>Actions</th>
            </tr>
            @foreach($levels as $level)
                <tr class="text-center">
                    <td class="text-left">{{substr($level->level, 0, 80)}}</td>
                    <td>{{$level->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                    <td>
                        <a href="{{route('admin.learninglevels.edit', [$level->id])}}">Edit</a> |
                        <a href="{{route('admin.learninglevels.deleteLevel', [$level->id])}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection