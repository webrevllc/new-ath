@extends('layout')

@section('content')

    <h1>Manage Categories</h1>
    <a class="admin_btn" href="#">Add New</a>
    <div class="container">

        <div class="clearfix"></div>
        <table class="admin-manage-table">
            <tr>
                <th>Category</th>
                <th>URL</th>
                <th>Deleted</th>
                <th>Actions</th>
            </tr>
            @foreach($categories as $category)
                <tr class="text-center">
                    <td class="text-left"><a href="/app/{{$category->url_name}}">{{substr($category->name, 0, 80)}}</a></td>
                    <td class="text-left">{{$category->url_name}}</td>
                    <td>{{$category->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                    <td><a href="#">Edit</a> | <a href="#">Delete</a></td>
                </tr>
                @foreach($category->subcategories as $subcategory)
                    <tr class="text-center">
                        <td class="text-left"><a href="/app/{{$subcategory->url_name}}">--{{substr($subcategory->name, 0, 80)}}</a></td>
                        <td class="text-left">{{$subcategory->url_name}}</td>
                        <td>{{$subcategory->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                        <td><a href="#">Edit</a> | <a href="#">Delete</a></td>
                    </tr>
                @endforeach
            @endforeach
        </table>
    </div>

@endsection