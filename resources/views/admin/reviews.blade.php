@extends('layout')

@section('header')
    <link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css" rel="stylesheet" />
@endsection

@section('content')

    <h1>Approve App Reviews</h1>

    <div class="container">

        <div class="clearfix"></div>
        <table class="admin-manage-table table table-bordered" id="theTable">
            <thead>
                <tr>
                    <th>Name</th>
                    {{--<th>Created By</th>--}}
                    {{--<th>Added By</th>--}}
                    <th>Actions</th>
                    <th>Created</th>
                </tr>
            </thead>
            <tbody>
                @foreach($reviews as $review)
                    @if(count($review))
                        <tr class="text-center">
                            <td class="text-left col-md-6" colspan="1"><a href="/app/{{$review->url_name}}">{{$review->name}}</a></td>
                            <td class="col-md-3"><a href="{{route('admin.apps.edit', [$review->id])}}">Edit</a> | <a href="{{route('admin.apps.deleteApp', [$review->id])}}">Delete</a> |
                                @if($review->is_approved)
                                    <a href="{{route('admin.apps.cancelApproval', [$review->id])}}">Active</a>
                                @else
                                    <a href="{{route('admin.apps.approveApp', [$review->id])}}">Inactive</a>
                                @endif
                            </td>
                            <td class="col-md-3">{{date('F j, Y', strtotime($review->created_at))}}</td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('footer')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#theTable').DataTable({
                "order": [[2, "desc"]]
            });
        } );
    </script>
@endsection