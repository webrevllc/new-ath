@extends('layout')

@section('content')
    <div class="">
        @if(Session::has('success'))
            <div class="alert alert-success span9">
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-error span9">
                {{Session::get('error')}}
            </div>
        @endif
            <div class="clearfix"></div>
        <h1>Manage Testimonials</h1>
        <h2>{{$app->name}}</h2>
        <div class="clearfix"></div>
            <a class="admin_btn" href="#">Add New</a>
            <div class="container">

            </div>
    </div>


@endsection