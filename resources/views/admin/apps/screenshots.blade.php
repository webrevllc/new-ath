@extends('layout')

@section('content')
    <div class="">
        @if(Session::has('success'))
            <div class="alert alert-success span9">
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-error span9">
                {{Session::get('error')}}
            </div>
        @endif
            <div class="clearfix"></div>
        <h1>Manage Screenshots</h1>
        <h2>{{$app->name}}</h2>
        <div class="clearfix"></div>
            <a class="admin_btn" href="#">Add New</a>
            <div class="container">
                @if(count($app->screenshots) > 0)
                    <table class='admin-manage-table'>
                        <tr>
                            <th>Image</th>
                            <th>Caption</th>
                            <th>Actions</th>
                        </tr>

                        @foreach($app->screenshots as $screenshot)
                            <tr>
                                <td><img src="/files/images/versions/small/app_screenshots/{{$screenshot->name.'-'.$screenshot->id.'.'.$screenshot->extension}}" alt="" /></td>
                                <td>{{$screenshot->caption}}</td>
                                <td>Edit Delete</td>
                            </tr>
                        @endforeach
                    </table>
                @else
                <h4>Sorry, no screenshots available. Please add a new one.</h4>
                @endif
            </div>
        </div>


@endsection