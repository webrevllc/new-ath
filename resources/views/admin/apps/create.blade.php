@extends('layout')

@section('content')
    <div class="">
        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('success') }}</h4>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <h4>{{ Session::get('error') }}</h4>
            </div>
        @endif
            <div class="clearfix"></div>
            @if(auth()->user()->hasRole('reviewer'))
                <a href="/account">Account</a> &raquo; Create A Review
            @elseif(auth()->user()->hasRole('admin'))
                <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/apps">Apps</a> &raquo; Create
            @endif
        <h1>Create a New App Review</h1>
        <div class="form">

            <form enctype="multipart/form-data" id="app-edit-form" action="{{route('admin.apps.store')}}" method="post" class="col-md-12">

                {{csrf_field()}}

                <p class="note">Fields with <span class="required">*</span> are required.</p>
                <div class="">
                    <label for="App_name" class="required"><h4>Name <span class="required">*</span></h4></label>
                    <input name="App[name]" id="App_name" type="text" maxlength="255" class="form-control" value="" />
                </div>

                @can('manage_site')
                    <div class="">
                        <label for="App_url_name"><h4>Url Name</h4></label>
                        <input name="App[url_name]" id="App_url_name" type="text" class="form-control" maxlength="255" value="" />
                    </div>
                @endcan

                <div class="" id="resizable">
                    <label for="App_description" class="required"><h4>Description <span class="required">*</span></h4></label>
                    <textarea name="App[description]"  class="form-control"></textarea>
                </div>

                <div class="">
                    <label for="App_developer_id"><h4>Developer</h4></label>
                    <select name="App[developer_id]" id="App_developer_id" class="form-control">
                        <option value="">(Select a Developer)</option>
                        @foreach($developers as $developer)
                            @if(! $developer->is_deleted)
                                <option value="{{$developer->id}}">{{$developer->name}}</option>
                            @endif
                        @endforeach

                    </select>
                </div>
                <br>
                <a href="/admin/developers/create">+ Add a New Developer</a>

                <div class="">
                    <label for="App_price"><h4>Price</h4></label>
                    <input name="App[price]" id="App_price" type="text" maxlength="19" value="" class="form-control"/>
                </div>



                <div id="category-list" class=" checkboxes_form_input checkbox-col span3" style="margin-left:0px" ng-controller="catController">
                    @include('admin.partial.category')
                </div>


                {{--<div id="subcategory-list" >--}}
                {{--<div class=" checkboxes_form_input checkbox-col" id="sublist-1">--}}
                {{----}}
                {{--@foreach($subs as $sub)--}}

                {{--<span id="AppSubcategory_subcategory_id">--}}
                {{--<div><label for="AppSubcategory_subcategory_id_0">{{$sub->name}}</label>--}}
                {{--<input value="{{$sub->id}}" id="AppSubcategory_subcategory_id_0" type="checkbox" name="AppSubcategory[subcategory_id][]" />--}}
                {{--</div>--}}
                {{--</span>--}}
                {{--@endforeach--}}

                {{--</div>--}}
                {{--</div>--}}
                <div class="clear-div"></div>
                <br>
                <br>
                <div class=" checkboxes_form_input">
                    <h4>Age Range</h4>
                <span id="AppAgerange_agerange_id">
                    @foreach($ages as $age)
                        <div>
                            <label for="AppAgerange_agerange_{{$age->id}}">{{$age->range}}</label>
                            <input value="{{$age->id}}" id="AppAgerange_agerange_{{$age->id}}" type="checkbox" name="AppAgerange[{{$age->id}}]" />
                        </div>
                    @endforeach
                </span>
                </div>
                <br>
                <br>
                @can('manage_site')
                <h4>Featured App?</h4>
                <label class="radio">
                    <input type="radio" name="App[is_featured]" id="optionsRadios1" value="true">
                    Yes
                </label>
                <label class="radio">
                    <input type="radio" name="App[is_featured]" id="optionsRadios2" value="false">
                    No
                </label>
                <br>
                <h4>Deleted?</h4>
                <label class="radio">
                    <input type="radio" name="App[is_deleted]" id="optionsRadios1" value="true">
                    Yes
                </label>
                <label class="radio">
                    <input type="radio" name="App[is_deleted]" id="optionsRadios2" value="false">
                    No
                </label>
                <br>
                <h4>Approved?</h4>
                <label class="radio">
                    <input type="radio" name="App[is_approved]" id="optionsRadios1" value="true">
                    Yes
                </label>
                <label class="radio">
                    <input type="radio" name="App[is_approved]" id="optionsRadios2" value="false">
                    No
                </label>
                <br>
                @endcan

                <div class="">
                    <label for="App_accolades"><h4>Accolades</h4></label>
                    <input name="App[accolades]" id="App_accolades" type="text" maxlength="255" value="" class="form-control"/>
                </div>

                <div class="">
                    <label for="App_features"><h4>Features</h4></label>
                    <textarea name="App[features]"  class="form-control"></textarea>
                </div>

                <div class="">
                    <label for="App_youtube_url"><h4>YouTube Trailer URL</h4></label>
                    <input name="App[youtube_url]" id="App_youtube_url" type="text" maxlength="255" class="form-control" value="" />
                </div>

                <div class="">
                    <label for="ReviewVideo_title"><h4>Review Video Title</h4></label>
                    <input name="ReviewVideo[title]" id="ReviewVideo_title" type="text" maxlength="255" class="form-control" value="" />
                </div>

                <div class="">
                    <label for="ReviewVideo_url"><h4>Review Video URL</h4></label>
                    <input name="ReviewVideo[url]" id="ReviewVideo_url" type="text" maxlength="255" class="form-control" value="" />
                </div>

                <div class="">
                    <label for="App_itunes_url"><h4>iTunes URL</h4></label>
                    <input name="App[itunes_url]" id="App_itunes_url" type="text" maxlength="255" class="form-control" value="" />
                </div>

                <div class="">
                    <label for="App_thumb_image_id"><h4>Thumbnail Image</h4></label>

                    <input name="thumbnail" id="App_thumb_image_id" type="file" />
                </div>

                {{--<div class="">--}}
                    {{--Testimonials <a href="/admin/testimonials/291/new">[Add New]</a><br>--}}
                {{--</div>--}}

                {{--<div class="">--}}
                    {{--Screenshots: <a href="/admin/screenshots/291/new">[Add New]</a><br>--}}
                {{--</div>--}}

                <div class=" buttons">
                    <input type="submit" name="yt0" value="Submit" class="btn btn-xs pull-right"/>	</div>

            </form>
        </div><!-- form -->
    </div>


@endsection

@section('footer')
    <script>
        app.controller('catController', function($http, $scope){
            $scope.categoryArray = [];
            var responsePromise = $http.get("/admin/getCats/");
            responsePromise.success(function(data, status, headers, config) {
                $scope.categories = data;
                console.log($scope.categories);
            });
            responsePromise.error(function(data, status, headers, config) {
//                    console.log(data);
            });

            $scope.add = function(c){
                if(c.selected == true){
                    $scope.categoryArray.push(c.id);
                }else{
                    var index = $scope.categoryArray.indexOf(c.id);
                    $scope.categoryArray.splice(index, 1);
                }

                $scope.getSubs();
            };

            $scope.getSubs = function(){
                console.log($scope.categoryArray);
                var responsePromise = $http.post("/admin/getSubs/", {'cats' : $scope.categoryArray});
                responsePromise.success(function(data, status, headers, config) {
                    $scope.subs = data;
                    console.log($scope.subs);
                });
                responsePromise.error(function(data, status, headers, config) {
                    console.log(data);
                });
            };

        });
    </script>
    @endsection