@extends('layout')

@section('content')
    <div class="form-group">
        @if(Session::has('success'))
            <div class="alert alert-success span9">
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-error span9">
                {{Session::get('error')}}
            </div>
        @endif
            <div class="clearfix"></div>
            @if(auth()->user()->hasRole('reviewer'))
                <a href="/account">Account</a> &raquo; <a href="/admin/my-reviews">My Reviews</a> &raquo; Edit
            @elseif(auth()->user()->hasRole('admin'))
                <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/apps">Apps</a> &raquo; Edit
            @endif

        <h1>Edit - {{$app->name}}</h1>
        <div class="form">
            <form enctype="multipart/form-data" id="app-edit-form" action="{{route('admin.apps.update', [$app->id])}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PUT">

                <p class="note">Fields with <span class="required">*</span> are required.</p>
                <div class="form-group">
                    <label for="App_name" class="required"><h4>Name<span class="required">*</span></h4> </label>
                    <input name="App[name]" id="App_name" type="text" maxlength="255" class="form-control" value="{{$app->name}}" />
                </div>

                @can('manage_site')
                    <div class="form-group">
                        <label for="App_url_name"><h4>URL Name<span class="required">*</span></h4></label>
                        <input name="App[url_name]" id="App_url_name" type="text" class="form-control" maxlength="255" value="{{$app->url_name}}" />
                    </div>
                @endcan

                <div class="form-group" >
                    <label for="App_descriptions" class="required"><h4>Description<span class="required">*</span></h4> </label>
                    <textarea name="App[description]" id="App_descriptions" rows="15" class="form-control">{!! $app->description !!}</textarea>
                </div>

                <div class="form-group">
                    <label for="App_developer_id"><h4>Developer</h4></label>
                    <select name="App[developer_id]" id="App_developer_id" class="form-control">
                        <option value="">(Select a Developer)</option>
                        @foreach($developers as $developer)
                            <option value="{{$developer->id}}"
                                    @if($developer->id == $app->developer_id)
                                    selected="selected"
                                    @endif
                            >{{$developer->name}}</option>
                        @endforeach

                    </select>
                </div>

                <div class="form-group">
                    <label for="App_price"><h4>Price</h4></label>
                    <input name="App[price]" id="App_price" type="text" maxlength="19" value="{{$app->price}}" class="form-control"/>
                </div>


                <div id="category-list" class=" checkboxes_form_input checkbox-col col-md-3" style="margin-left:0px">
                    <label for="AppCategory[category_id]"><h4>Category</h4></label>
                <span id="AppCategory_category_id">
                    @foreach($categories as $category)
                        @if(! $category->is_deleted)
                            <div>
                                <label for="AppCategory_category_{{$category->id}}">{{$category->name}}</label>
                                <input value="1" id="AppCategory_category_{{$category->id}}"
                                       @if(count($app->categories) > 0)
                                           @foreach($app->categories as $appCats)
                                               @if($appCats->name == $category->name)
                                               checked="checked"
                                               @endif
                                           @endforeach
                                       @endif
                                       type="checkbox" name="AppCategory[{{$category->id}}]" />
                            </div>
                        @endif
                    @endforeach
                </span>
                </div>
                @if(count($app->categories) > 0)
                    @foreach($app->categories as $appCat)
                        @foreach($categories as $cats)
                            @if($appCat->id == $cats->id && count($cats->subcategories) > 0)
                                <div id="category-list" class=" checkboxes_form_input checkbox-col col-md-3">
                                    <h4>{{$appCat->name}}</h4>
                                    @foreach($cats->subcategories as $subcats)
                                        <div>
                                    <span id="AppSubcategory_subcategory_id col-md-3 pull-left">
                                        <label for="AppSubcategory_subcategory_{{$subcats->id}}">{{$subcats->name}}</label>
                                        <input value="{{$subcats->id}}" id="AppSubcategory_subcategory_{{$subcats->id}}" type="checkbox"
                                               @if(count($app->categories) > 0)
                                                   @foreach($app->subcategories as $appSubs)
                                                       @if($appSubs->name == $subcats->name)
                                                       checked="checked"
                                                       @endif
                                                   @endforeach
                                               @endif
                                               name="AppSubcategory[{{$subcats->id}}]"
                                        />
                                    </span>
                                        </div>
                                    @endforeach
                                </div>

                            @endif
                        @endforeach
                    @endforeach
                @endif
                {{--<div id="subcategory-list" >--}}
                {{--<div class=" checkboxes_form_input checkbox-col" id="sublist-1">--}}
                {{----}}
                {{--@foreach($subs as $sub)--}}

                {{--<span id="AppSubcategory_subcategory_id">--}}
                {{--<div><label for="AppSubcategory_subcategory_id_0">{{$sub->name}}</label>--}}
                {{--<input value="{{$sub->id}}" id="AppSubcategory_subcategory_id_0" type="checkbox" name="AppSubcategory[subcategory_id][]" />--}}
                {{--</div>--}}
                {{--</span>--}}
                {{--@endforeach--}}

                {{--</div>--}}
                {{--</div>--}}
                <div class="clear-div"></div>
                <br>
                <br>
                <div class=" checkboxes_form_input">
                    <h4>Age Range</h4>
                <span id="AppAgerange_agerange_id">
                    @foreach($ages as $age)
                        <div>
                            <label for="AppAgerange_agerange_{{$age->id}}">{{$age->range}}</label>
                            <input value="{{$age->id}}" id="AppAgerange_agerange_{{$age->id}}" type="checkbox"
                                   @if(count($app->categories) > 0)
                                       @foreach($app->ageRanges as $appRange)
                                           @if($appRange->range == $age->range)
                                           checked="checked"
                                           @endif
                                       @endforeach
                                   @endif
                                   name="AppAgerange[{{$age->id}}]" />
                        </div>
                    @endforeach
                </span>
                </div>
                <br>
                <br>
                @can('manage_site')
                <h4>Featured App?</h4>
                    <label class="radio">
                        <input type="radio" name="App[is_featured]" id="optionsRadios1" value="true" {{$app->is_featured ? 'checked' : ''}}>
                        Yes
                    </label>
                    <label class="radio">
                        <input type="radio" name="App[is_featured]" id="optionsRadios2" value="false" {{$app->is_featured ? '' : 'checked'}}>
                        No
                    </label>
                <br>
                <h4>Deleted?</h4>
                    <label class="radio">
                        <input type="radio" name="App[is_deleted]" id="optionsRadios1" value="true" {{$app->is_deleted ? 'checked' : ''}}>
                        Yes
                    </label>
                    <label class="radio">
                        <input type="radio" name="App[is_deleted]" id="optionsRadios2" value="false" {{$app->is_deleted ? '' : 'checked'}}>
                        No
                    </label>
                <br>
                <h4>Approved?</h4>
                <label class="radio">
                    <input type="radio" name="App[is_approved]" id="optionsRadios1" value="true" {{$app->is_approved ? 'checked' : ''}}>
                    Yes
                </label>
                <label class="radio">
                    <input type="radio" name="App[is_approved]" id="optionsRadios2" value="false" {{$app->is_approved ? '' : 'checked'}}>
                    No
                </label>
                <br>
                @endcan

                <div class="form-group">
                    <label for="App_accolades"><h4>Accolades</h4></label>
                    <textarea name="App[accolades]" id="App_accolades" class="form-control" rows="15">{!! $app->accolades !!}</textarea>
                </div>
                <br>
                <div class="form-group">
                    <label for="App_featuress"><h4>Features</h4></label>
                    <textarea name="App[features]" id="App_featuress" rows="5" class="form-control">{!! $app->features !!}</textarea>
                </div>
                <br>
                <div class="form-group">
                    <label for="App_youtube_url"><h4>YouTube Trailer URL</h4></label>
                    <input name="App[youtube_url]" id="App_youtube_url" type="text" maxlength="255" class="form-control" value="{{$app->youtube_url}}" />
                </div>
                <br>
                <div class="form-group">
                    <label for="ReviewVideo_title"><h4>Review Video Title</h4></label>
                    <input name="ReviewVideo[title]" id="ReviewVideo_title" type="text" maxlength="255" class="form-control" value="{{count($app->review) > 0 ? $app->review->title : ''}}" />
                </div>
                <br>
                <div class="form-group">
                    <label for="ReviewVideo_url"><h4>Review Video URL</h4></label>
                    <input name="ReviewVideo[url]" id="ReviewVideo_url" type="text" maxlength="255" class="form-control" value="{{count($app->review) > 0 ? $app->review->url : ''}}" />
                </div>
                <br>
                <div class="form-group">
                    <label for="App_itunes_url"><h4>iTunes URL</h4></label>
                    <input name="App[itunes_url]" id="App_itunes_url" type="text" maxlength="255" class="form-control" value="{{$app->itunes_url}}" />
                </div>
                <br>
                <div class="form-group">
                    <label for="App_thumb_image_id"><h4>Thumbnail Image</h4></label>
                    @if($app->thumb_image_id != '')
                        <?php $thumb = getThumbs($app->thumb_image_id) ?>
                    <br>
                        <img src="/files/images/versions/small/app_thumbs/{{$thumb->name.'-'.$thumb->id.'.'.$thumb->extension}}" alt="" />
                    @endif
                    {{--<input id="ytApp_thumb_image_id" type="hidden" value="" name="App[thumb_image_id]" />--}}
                    <input name="thumbnail" id="App_thumb_image_id" type="file" class="form-control"/>
                </div>
                <br>
                <div class="form-group">
                    Testimonials <a href="{{route('admin.apps.testimonial', [$app])}}">[Add New]</a><br>
                </div>

                <div class="form-group">
                    Screenshots: <a href="{{route('admin.apps.screenshots', [$app])}}">[Add New]</a><br>
                </div>

                <div class=" pull-right">
                    <input type="submit" name="yt0" value="Submit" />	</div>
            </form>
        </div><!-- form -->
    </div>



@endsection