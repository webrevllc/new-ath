@extends('layout')

@section('header')
    <link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css" rel="stylesheet" />
@endsection

@section('content')
    <a href="/account">Account</a> &raquo; My Reviews
    <div class="clearfix"></div>
    @if(Session::has('success'))
    <div class="alert alert-success span9">
        {{Session::get('success')}}
    </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-error span9">
            {{Session::get('error')}}
        </div>
    @endif
    <div class="clearfix"></div>
<h1>My Reviews</h1>
<a class="admin_btn" href="{{route('admin.apps.create')}}">Add New</a>
<div class="container">

    <div class="clearfix"></div>
    <table class="table table-bordered" id="theTable">
        <thead>
            <tr>
                <th>Name</th>
                {{--<th>Created By</th>--}}
                <th>Deleted</th>
                <th>Approved</th>
                <th>Featured</th>
                <th>Actions</th>
                <th>Created</th>
            </tr>
        </thead>
        <tbody>
            @foreach($apps as $app)
                <tr class="text-center">
                    <td class="text-left"><a href="/app/{{$app->url_name}}">{{substr($app->name, 0, 80)}}</a></td>
                    {{--<td></td>--}}
                    <td>{{$app->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                    <td>{{$app->is_approved == 1 ? 'Approved' : 'No'}}</td>
                    <td>{{$app->is_featured == 1 ? 'Featured' : 'No'}}</td>
                    <td>
                        <a href="{{route('admin.apps.edit', [$app->id])}}">Edit</a>
                        {{--<a href="{{route('admin.apps.deleteApp', [$app->id])}}">Delete</a> |--}}
                        {{--<a href="{{route('admin.apps.testimonial', [$app->id])}}">Add Testimonial</a>--}}
                    </td>
                    <td class="col-md-2">{{date('F j, Y', strtotime($app->created_at))}}</td>
                </tr>
            @endforeach
        </tbody>

    </table>
</div>

@endsection

@section('footer')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#theTable').DataTable({
                "order": [[5, "desc"]]
            });
        } );
    </script>
@endsection