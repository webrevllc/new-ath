@extends('layout')

@section('content')
    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; Subcategories
    <h1>Manage Categories</h1>
    <a class="admin_btn" href="#">Add New</a>
    <div class="container">

        <div class="clearfix"></div>
        <table class="admin-manage-table">
            <tr>
                <th>Category</th>
                <th>URL</th>
                <th>Deleted</th>
                <th>Actions</th>
            </tr>
            @foreach($categories as $category)
                <tr class="text-center">
                    <td class="text-left"><a href="/category/{{$category->url_name}}">{{substr($category->name, 0, 80)}}</a></td>
                    <td class="text-left">{{$category->url_name}}</td>
                    <td>{{$category->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                    <td class="text-left">
                        <a href="{{route('admin.categories.edit', [$category->id])}}">Edit</a> |
                        <a href="{{route('admin.categories.deleteCategory', [$category->id])}}">Delete</a> |
                        <a href="{{route('admin.categories.addSub', [$category->id])}}">Add Subcategory</a>
                    </td>
                </tr>
                @foreach($category->subcategories as $subcategory)
                    <tr class="text-center">
                        <td class="text-left"><a href="/subcategory/{{$subcategory->url_name}}">--{{substr($subcategory->name, 0, 80)}}</a></td>
                        <td class="text-left">{{$subcategory->url_name}}</td>
                        <td>{{$subcategory->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                        <td class="text-left">
                            <a href="{{route('admin.subcategories.edit', [$subcategory->id])}}">Edit</a> |
                            <a href="{{route('admin.subcategories.deleteSubcategory', [$subcategory->id])}}">Delete</a></td>
                    </tr>
                @endforeach
            @endforeach
        </table>
    </div>

@endsection