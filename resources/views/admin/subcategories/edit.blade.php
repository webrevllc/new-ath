@extends('layout')

@section('content')

    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/categories">Categories</a> &raquo; Edit
    <h1>Manage {{$category->name}}</h1>
    <div class="container">
        <div class="form">
            @if(Session::has('success'))
                <div class="alert alert-success span9">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error span9">
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="clearfix"></div>
            <form id="developer-edit-form" action="{{route('admin.subcategories.update', [$category->id])}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PUT">
                <div class="row">
                    <p class="note">Fields with <span class="required">*</span> are required.</p>
                </div>

                <div class="row">
                    <label for="Developer_name" class="required">Name <span class="required">*</span></label>
                    <input name="Category[name]" id="Developer_name" type="text" maxlength="255" value="{{$category->name}}" />
                </div>

                <div class="row">
                    <label for="Developer_url_name">Url Name</label>
                    <input name="Category[url_name]" id="Developer_url_name" type="text" maxlength="255" value="{{$category->url_name}}" />
                </div>

                <div class="row">
                    <label for="Developer_is_deleted">Is Deleted</label>
                    <input name="Category[is_deleted]" id="Developer_is_deleted" type="text" value="{{$category->is_deleted}}" />
                </div>

                <div class="row buttons">
                    <input type="submit" name="yt0" value="Submit" />	</div>

            </form>
        </div><!-- form -->

    </div>
@endsection