@extends('layout')

@section('content')

    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/categories">Categories</a> &raquo; Create
    <h1>Add Subcategory to {{$category->name}}</h1>
    <div class="container">
        <div class="form">
            @if(Session::has('success'))
                <div class="alert alert-success span9">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error span9">
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="clearfix"></div>
            <form id="developer-edit-form" action="{{route('admin.subcategories.make', [$category->id])}}" method="post">
                {{csrf_field()}}
                <div class="control-group">
                    <p class="note">Fields with <span class="required">*</span> are required.</p>
                </div>

                <div class="control-group">
                    <label for="Developer_name" class="required">Name <span class="required">*</span></label>
                    <input name="Category[name]" id="Developer_name" class="form-control" type="text" maxlength="255" required value="" />
                </div>
                <br>
                <div class="control-group">
                    <label for="Developer_url_name">Url Name <span class="required">*</span></label>
                    <input name="Category[url_name]" id="Developer_url_name" type="text" class="form-control" required maxlength="255" value="" />
                </div>
                <br>
                <div class="control-group">
                    <input type="submit" class="pull-right" name="yt0" value="Submit" />	</div>

            </form>
        </div><!-- form -->

    </div>
@endsection