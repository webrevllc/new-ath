<style>
    .subs{
        background:#efefef;
        padding:0px 15px 15px 15px;
        margin-top:10px;
    }
</style>
<div class="row">
    <div class="col-md-4">
        <h1>Categories</h1>
        <div class="checkbox" ng-repeat="c in categories">
            <label>
                <input type="checkbox" ng-model="c.selected" value='<% c.id %>' ng-change="add(c)" name="AppCategory[<% c.id %>]"/> <% c.name %>
            </label>
        </div>
    </div>
    <div ng-if="subs" class="subs col-md-8">
        <h1>Subcategories</h1>
        <div  ng-repeat="s in subs">
            <h2><% s.name %></h2>
            <div class="checkbox" ng-repeat="thesub in s.subcategories">
                <label>
                    <input type="checkbox" ng-model="thesub.selected" value='<% thesub.id %>' name="AppSubcategory[<% thesub.id %>]" /> <% thesub.name %>
                </label>
            </div>
        </div>
    </div>
</div>





