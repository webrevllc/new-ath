@extends('layout')

@section('content')

    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/ageranges">Age Ranges</a> &raquo; Create
    <h1>Create a New Age Range</h1>
    <div class="container">
        <div class="form">
            @if(Session::has('success'))
                <div class="alert alert-success span9">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error span9">
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="clearfix"></div>
            <form id="developer-edit-form" action="{{route('admin.ageranges.store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                    <p class="note">Fields with <span class="required">*</span> are required.</p>
                </div>

                <div class="form-group">
                    <label for="Developer_name" class="required">Name <span class="required">*</span></label>
                    <input name="Range[range]" id="Developer_name" type="text" maxlength="255" value="" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="Developer_url_name">Url Name</label>
                    <input name="Range[url_name]" id="Developer_url_name" type="text" maxlength="255" value="" class="form-control"/>
                </div>

                <div class="row buttons">
                    <input type="submit" name="yt0" value="Submit" class="btn pull-right"/>	</div>

            </form>
        </div><!-- form -->

    </div>
@endsection