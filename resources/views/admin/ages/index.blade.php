@extends('layout')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success span9">
            {{Session::get('success')}}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-error span9">
            {{Session::get('error')}}
        </div>
    @endif
    <div class="clearfix"></div>
    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; Age Ranges
    <h1>Manage Age Ranges</h1>
    <a class="admin_btn" href="{{route('admin.ageranges.create')}}">Add New</a>
    <div class="container">
        <table class="table table-bordered table-condensed">
            <tr>
                <th>Age Range</th>
                <th>Deleted</th>
                <th>Actions</th>
            </tr>
            @foreach($ages as $age)
                <tr class="text-center">
                    <td class="text-left"><a href="/app/agerange/{{$age->id}}">{{substr($age->range, 0, 80)}}</a></td>
                    <td>{{$age->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                    <td><a href="{{route('admin.ageranges.edit', [$age->id])}}">Edit</a> |
                        <a href="{{route('admin.ageranges.deleteRange', [$age->id])}}">Delete</a></td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection