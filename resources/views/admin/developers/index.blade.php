@extends('layout')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success span9">
            {{Session::get('success')}}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-error span9">
            {{Session::get('error')}}
        </div>
    @endif
    <div class="clearfix"></div>
    <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; Developers
    <h1>Manage Developers</h1>
    <a class="admin_btn" href="{{route('admin.developers.create')}}">Add New</a>
    <div class="container">

        <div class="clearfix"></div>
        <table class="table table-bordered table-condensed">
            <tr>
                <th>Developer</th>
                <th>URL</th>
                <th>Deleted</th>
                <th>Actions</th>
            </tr>
            @foreach($developers as $developer)
                <tr class="text-center">
                    <td class="text-left"><a href="/developer/{{$developer->url_name}}">{{substr($developer->name, 0, 80)}}</a></td>
                    <td class="text-left">{{$developer->url_name}}</td>
                    <td>{{$developer->is_deleted == 1 ? 'Deleted' : 'No'}}</td>
                    <td><a href="{{route('admin.developers.edit', [$developer->id])}}">Edit</a> | <a href="{{route('admin.developers.deleteDeveloper', [$developer->id])}}">Delete</a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection