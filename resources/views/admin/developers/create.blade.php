@extends('layout')

@section('content')


    <div class="container">
        <div class="form-group">
            <div class="clearfix"></div>
            @if(Session::has('success'))
                <div class="alert alert-success span9">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error span9">
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="clearfix"></div>
            @can('manage_site')
                <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/developers">Developers</a> &raquo; Create
            @endcan

            <h1>Create a New Developer</h1>
        </div>


            <div class="clearfix"></div>
            <form id="developer-edit-form" action="{{route('admin.developers.store')}}" method="post" class="col-md-6">
                {{csrf_field()}}
                <div class="l">
                    <p class="note">Fields with <span class="required">*</span> are required.</p>
                </div>

                <div class="form-group">
                    <label for="Developer_name" class="required">Name <span class="required">*</span></label>
                    <input name="Developer[name]" id="Developer_name" type="text" maxlength="255" value="" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="Developer_url_name">Url Name</label>
                    <input name="Developer[url_name]" id="Developer_url_name" type="text" maxlength="255" value="" class="form-control"/>
                </div>
                <div class="row pull-right">
                    <input type="submit" name="yt0" value="Submit" />	</div>

            </form>

    </div>
@endsection