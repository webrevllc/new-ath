@extends('layout')

@section('content')

    @can('manage_site')
        <a href="/account">Account</a> &raquo; <a href="/account/dashboard">Dashboard</a> &raquo; <a href="/admin/developers">Developers</a> &raquo; Edit
    @endcan

    <h1>Manage {{$developer->name}}</h1>
    <div class="container">
        <div class="form">
            @if(Session::has('success'))
                <div class="alert alert-success span9">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error span9">
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="clearfix"></div>
            <form id="developer-edit-form" action="{{route('admin.developers.update', [$developer->id])}}" method="post" class="col-md-6">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PUT">
                <div class="row">
                    <p class="note">Fields with <span class="required">*</span> are required.</p>
                </div>

                <div class="form-group">
                    <label for="Developer_name" class="required">Name <span class="required">*</span></label>
                    <input name="Developer[name]" id="Developer_name" type="text" maxlength="255" value="{{$developer->name}}" class="form-control" required/>
                </div>

                <div class="form-group">
                    <label for="Developer_url_name">Url Name</label>
                    <input name="Developer[url_name]" id="Developer_url_name" type="text" maxlength="255" value="{{$developer->url_name}}" class="form-control" required/>
                </div>

                <div class="form-group">
                    <label for="Developer_is_deleted">Is Deleted</label>
                    <input name="Developer[is_deleted]" id="Developer_is_deleted" type="text" value="{{$developer->is_deleted}}" class="form-control" />
                </div>

                <div class="form-group">
                    <input type="submit" name="yt0" value="Submit" />	</div>

            </form>
        </div><!-- form -->

    </div>
@endsection