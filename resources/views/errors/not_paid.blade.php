@extends('layout')

@section('content')
    <h2>In order to access that area of the site you must be a premium user. For only $10 you can have access to this area plus many others!</h2>
    <form id="checkout" method="post" action="/checkout">
        {{csrf_field()}}
        <div id="payment-form"></div>
        <button class="btn btn-primary pull-right" type="submit">Sign Up Now</button>
    </form>

    <script src="https://js.braintreegateway.com/js/braintree-2.21.0.min.js"></script>
    <script>
        // We generated a client token for you so you can test out this code
        // immediately. In a production-ready integration, you will need to
        // generate a client token on your server (see section below).
        var clientToken = "{{\Braintree_ClientToken::generate()}}";

        braintree.setup(clientToken, "dropin", {
            container: "payment-form",
            paypal: {
                button: {
                    type: 'checkout'
                }
            }
        });
    </script>
@endsection