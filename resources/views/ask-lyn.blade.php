@extends('layout')
@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h4>{{ Session::get('success') }}</h4>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <h4>{{ Session::get('error') }}</h4>
        </div>
    @endif
    <h1>Ask Lyn:</h1>
    <form class="form-horizontal col-md-6" action="/ask-lyn" method="post" >
        {{csrf_field()}}
        <div class="control-group">
            <label class="control-label" for="name">Name*:</label>
            <div class="controls">
                <input type="text" id="name" name="name" class="form-control">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="email">Email*:</label>
            <div class="controls">
                <input type="text" id="email" name="email" class="form-control">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputEmail">Question*:</label>
            <div class="controls">
                <textarea rows="7" name="question" class="form-control"></textarea>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Ask Lyn Now!</button>
    </form>

@stop