@extends('layout')
@section('content')

    <h1>Featured Apps</h1>

    <style>
        .col-md-4:nth-child(3n+1){
            clear:left;
        }
    </style>
    <div class="row-fluid">
        @foreach($apps as $app)
            <?php $thumb = getThumbs($app->thumb_image_id) ?>
            <div class="col-xs-12 col-md-4 featured-app">
                <div class="app-thumb">
                    <a href="/app/{{$app->url_name}}">
                        <img src="/files/images/versions/small/app_thumbs/{{$thumb->name.'-'.$thumb->id.'.'.$thumb->extension}}" alt="" width="100%"/>
                    </a>
                </div>
                <div class="app-info">
                    <div class="app-name app-info-item" style=''>
                        <a href="/app/{{$app->url_name}}">{{substr($app->name, 0, 30)}}
                            @if(strlen($app->name) > 30)
                                ...
                            @endif
                        </a>
                    </div>
                    <div class="cat-name app-info-item">
                        <?php $i = 0; $catCount = count($app->categories)?>
                        @foreach($app->categories as $category)
                            <?php if(++$i > 2) {
                                echo $catCount - 2 .' more';
                                break;
                            }?>
                            <a href="/category/{{$category->url_name}}">{{$category->name}}</a>,

                        @endforeach
                    </div>

                    <div class="ages app-info-item">
                        Ages:
                        @foreach($app->ageRanges as $ageRange)
                            <a href="/app/agerange/{{$ageRange->url_name}}">{{$ageRange->range}}</a>,
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop