@extends('layout')

@section('content')

<title>My Account |App Treasure Hunter</title>

<div class="row reviews">

    <div class="col-md-12">

        @if(Session::has('success'))
            <div class="alert alert-success">
               {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-error">
                {{Session::get('error')}}
            </div>
        @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h2>Change Your Password</h2>
            <form action="/account/password" method="post">
            {{csrf_field()}}
            <label>Enter a new Passsword</label>
            <input type="password" name="password" class="form-control">
                <br>
            <label>Confirm New Password</label>
            <input type="password" name="password_confirmation" class="form-control"><br>
            <button type="submit" class="pull-right">Submit</button>
        </form>
    </div>
    <!--end .span9-->

</div><!--end .row-->
@endsection