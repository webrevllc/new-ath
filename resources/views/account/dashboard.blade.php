@extends('layout')

@section('content')

<title>My Account | App Treasure Hunter</title>

<div class="row reviews">

    <div class="span12">
        <a href="/account">Account</a> &raquo; Dashboard
        <h1>Admin Dashboard</h1>
        <h2>Manage</h2>
        <p><a href="/admin/apps">Apps</a></p>
        <p><a href="/admin/developers">Developers</a></p>
        <p><a href="/admin/categories">Categories</a></p>
        <p><a href="/admin/learninglevels">Learning Levels</a></p>
        <p><a href="/admin/ageranges">Age Ranges</a></p>

        <h2>Approve and Authorize</h2>
        <p><a href="/admin/users">Users</a></p>
        <p><a href="/admin/reviews">Reviews</a></p>
        {{--<p><a href="/admin/videoreviews">Video Reviews</a></p>--}}

    </div>
    <!--end .span9-->

</div><!--end .row-->
@endsection