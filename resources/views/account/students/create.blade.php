@extends('layout')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="form">
        <a href="/account">Account</a> &raquo; Create a Student
        <h1>Add Student</h1>
        <form id="student-addstudent-form" action="{{route('students.store')}}" method="post">
            {{csrf_field()}}
            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <div class="form-group">
                <label for="Student_first_name" class="required">First Name <span class="required">*</span></label>
                <input name="Student[first_name]" id="Student_first_name" type="text" maxlength="255" class="form-control" required/>
            </div>

            <div class="form-group">
                <label for="Student_last_name" class="required">Last Name <span class="required">*</span></label>
                <input name="Student[last_name]" id="Student_last_name" type="text" maxlength="255" class="form-control" required/>
            </div>

            <div class="form-group">
                <label for="Student_birth_day">Birth Month</label>
                {{--<select class="form-control" name="Student[birth_month]">--}}
                    {{--<option value="">-- please select --</option>--}}
                    {{--<option value="1">January</option>--}}
                    {{--<option value="2">Februaru</option>--}}
                    {{--<option value="3">March</option>--}}
                    {{--<option value="4">April</option>--}}
                    {{--<option value="5">May</option>--}}
                    {{--<option value="6">June</option>--}}
                    {{--<option value="7">July</option>--}}
                    {{--<option value="8">August</option>--}}
                    {{--<option value="9">September</option>--}}
                    {{--<option value="10">October</option>--}}
                    {{--<option value="11">November</option>--}}
                    {{--<option value="12">December</option>--}}
                {{--</select>--}}
                {{ Form::selectMonth('Student[birth_month]', null,  ["placeholder" => "Select Month",'class' => 'form-control', 'required' => 'required']) }}
            </div>

            <div class="form-group">
                <label for="Student_birth_day">Birth Day</label>
                {{ Form::selectRange('Student[birth_day]', 1, 31, '', ["placeholder" => "Select Day", 'class' => 'form-control', 'required' => 'required']) }}
            </div>

            <div class="form-group">
                <label for="Student_birth_year">Birth Year</label>
                {{ Form::selectYear('Student[birth_year]', 2016, 1950, '', ["placeholder" => "Select Year", 'class' => 'form-control', 'required' => 'required']) }}
            </div>

            <div class="form-group">
                <label for="Student_subjects_of_interest">Subjects Of Interest</label>
                <textarea name="Student[subjects_of_interest]" id="Student_subjects_of_interest" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <label for="Student_subjects_of_difficulty">Subjects Of Difficulty</label>
                <textarea name="Student[subjects_of_difficulty]" id="Student_subjects_of_difficulty" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <label for="Student_learning_level">Learning Level</label>
                <input name="Student[learning_level]" id="Student_learning_level" type="text" maxlength="255" class="form-control"/>
            </div>

            <div class="form-group">
                <label for="Student_comments">Comments</label>
                <textarea name="Student[comments]" id="Student_comments" class="form-control"></textarea>
            </div>

            <div class="row buttons">
                <input type="submit" name="yt0" value="Submit" class="btn pull-right"/>
            </div>

        </form>
    </div><!-- form -->
@endsection