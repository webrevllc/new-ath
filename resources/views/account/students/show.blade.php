@extends('layout')

@section('content')
    <div class="row">
        <a href="/account">Account</a> &raquo; {{$student->first_name}} {{$student->last_name}}
        <h1>{{$student->first_name}} {{$student->last_name}}</h1>
    </div>
    <div class="col-md-6">


        <a href="/account/students/delete/{{$student->id}}">[delete]</a>
        <a href="{{route('students.edit', [$student->id])}}">[edit]</a>
        <br>
        <br>
        <div class="clearfix"></div>
        <div><strong>Age: {{getStudentAge($student)}}</strong></div>
        </br>

        <div><strong>Learning Level: {{$student->learning_level}}</strong></div>
        </br>

        <div><strong>Subjects of interest: {{$student->subjects_of_interest}}</strong></div>
        </br>

        <div><strong>Subjects of difficulty: {{$student->subjects_of_difficulty}}</strong></div>
        </br>
    </div>
    <div class="span7">
        <div ng-controller="studentApp">
        <div class="alert alert-success" ng-show="success">
            Students app successfully updated!
        </div>

            <strong>Followed Apps:</strong>
            {{--<select id="student-followed-apps">--}}
                {{--@foreach($student->apps as $app)--}}
                    {{--<option value="{{$app->id}}">{{$app->name}}</option>--}}
                {{--@endforeach--}}
            {{--</select>--}}
            {{Form::select('app',
                [],
                null,
                [
                    'ng-model' => 'selectedApp',
                    'ng-options' => 'app.name for app in apps track by app.id',
                    'placeholder' => 'Select App',

                ]
            )}}

            <div id='student-app-details' ng-if="selectedApp">
            <h3><% selectedApp.name %></h3>
            <form id='student-app-info-form'>
                <div>
                    <div>
                        <strong>Owned: </strong>
                        <input name="Owned"  type="checkbox" id="student-app-info-owned" title="Owned"  ng-model="selectedApp.pivot.is_owned"  ng-true-value="1" ng-false-value="0">
                    </div>

                    <div>
                        <strong>Comments on app:</strong>
                        <textarea id='student-app-info-comments' ng-model="selectedApp.pivot.comments"><% selectedApp.pivot.comments %></textarea>
                    </div>

                    <div>
                        <strong>Proficiency on app:</strong>
                        <input type='text' id='student-app-info-proficiency' value="" name='student-app-info-proficiency' ng-model="selectedApp.pivot.proficiency">

                    </div>
                    <button type='button' id='submit-student-app-info-btn' value="Save" ng-click="save(selectedApp)">Save</button>
                </div>
            </form>
        </div>
        </div>
    </div>
    <!--end .span9-->
@endsection

@section('footer')
    <script>
        var id = "{{$student->id}}"
        app.controller('studentApp', function($scope, $http){

            $scope.getUserApps = function(){
                var responsePromise = $http.get("/account/studentapps/" + id);
                responsePromise.success(function(data, status, headers, config) {
                    $scope.apps = data.apps;
                });
                responsePromise.error(function(data, status, headers, config) {
                    console.log(data);
                });
            };


            $scope.save = function(selectedApp) {
                $scope.success = false;
                var responsePromise = $http.post("/account/updateExistingStudentApp/" + selectedApp.pivot.student_id + '/' + selectedApp.pivot.app_id, {"data": selectedApp.pivot});
                responsePromise.success(function (data, status, headers, config) {
                    console.log(data);
                    $scope.apps = {};
                    $scope.success = true;
                    $scope.getUserApps();
                });
                responsePromise.error(function (data, status, headers, config) {
                    console.log(data);
                });
            };
            $scope.getUserApps();
            $scope.success = false;
            $scope.selectedApp = false;

//            $scope.showApp = function(app){
//                $scope.theApp = app;
//            }
        });
    </script>
@endsection