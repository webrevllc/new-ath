@extends('layout')

@section('content')
    <a href="/account">Account</a> &raquo; <a href="/account/profile">Profile</a> &raquo; Students
<h1>Edit Student</h1>
@if(Session::has('success'))
    <div class="alert alert-success span9">
        {{Session::get('success')}}
    </div>
@endif
@if(Session::has('error'))
    <div class="alert alert-error span9">
        {{Session::get('error')}}
    </div>
@endif
<div class="clearfix"></div>
<div class="form col-md-6">
    <form id="student-addstudent-form" action="{{route('students.update', [$student->id])}}" method="post">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PUT">
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="form-group">
        <label for="Student_first_name" class="required">First Name <span class="required">*</span></label>
        <input name="Student[first_name]" class="form-control"id="Student_first_name" type="text" maxlength="255" value="{{$student->first_name}}" />
    </div>

    <div class="form-group">
        <label for="Student_last_name" class="required">Last Name <span class="required">*</span></label>
        <input name="Student[last_name]" class="form-control"id="Student_last_name" type="text" maxlength="255" value="{{$student->last_name}}" />
    </div>

        <div class="form-group">
            <label for="Student_birth_day">Birth Month</label>
            {{ Form::selectMonth('Student[birth_month]', $student->birth_month, ["class" => "form-control"]) }}
        </div>

        <div class="form-group">
            <label for="Student_birth_day">Birth Day</label>
            {{ Form::selectRange('Student[birth_day]', 1, 31, $student->birth_day, ["placeholder" => "Select Day", "class" => "form-control"]) }}
        </div>

        <div class="form-group">
            <label for="Student_birth_year">Birth Year</label>
            {{ Form::selectYear('Student[birth_year]', 2016, 1950, $student->birth_year, ["placeholder" => "Select Year", "class" => "form-control"]) }}
        </div>

    <div class="form-group">
        <label for="Student_subjects_of_interest">Subjects Of Interest</label>
        <textarea name="Student[subjects_of_interest]" class="form-control" id="Student_subjects_of_interest">{{$student->subjects_of_interest}}</textarea>
    </div>

    <div class="form-group">
        <label for="Student_subjects_of_difficulty">Subjects Of Difficulty</label>
        <textarea name="Student[subjects_of_difficulty]" class="form-control" id="Student_subjects_of_difficulty">{{$student->subjects_of_difficulty}}</textarea>
    </div>

    <div class="form-group">
        <label for="Student_learning_level">Learning Level</label>
        <input name="Student[learning_level]" id="Student_learning_level" class="form-control" type="text" maxlength="255" value="{{$student->learning_level}}" />
    </div>

    <div class="form-group">
        <label for="Student_comments">Comments</label>
        <textarea name="Student[comments]" class="form-control" id="Student_comments">{{$student->comments}}</textarea>
    </div>

    <input type="submit" name="yt0" class="pull-right" value="Submit" />
</form>
</div><!-- form -->

    @endsection