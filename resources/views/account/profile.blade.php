@extends('layout')

@section('content')

<div class="row reviews">

    <div class="span12">
        <a href="/account">Account</a> &raquo; Profile
        <h1>{{$profile->first_name.' '. $profile->last_name}}</h1>
        <p>Age: {{getUserAge($profile)}}</p>
        <p>Country: {{$profile->country}}</p>
        <p>Gender: {{$profile->gender}}</p>
        <p>Interested for: {{$profile->interested_for}}</p>

        <h3>Students</h3>
        @foreach($students as $student)
            <h4><a href="{{route('students.show', [$student->id])}}">{{$student->first_name.' '. $student->last_name}}</a></h4>
            <div>Age: {{getStudentAge($student)}}</div>
            <div>Learning Level: {{$student->learning_level}}</div>
            <div>Apps owned:
                <ul>
                    @foreach($student->apps as $app)
                        <li><a href="/app/{{$app->url_name}}">{{$app->name}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div>Subjects of interest: {{$student->subjects_of_interest}}</div>
            <div>Comments: {{$student->comments}}</div>
        @endforeach
    </div>
    <!--end .span9-->

</div><!--end .row-->
@endsection