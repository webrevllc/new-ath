@extends('layout')

@section('content')

    <div class="row">
        {{--<a href="#" class="pull-right clearfix">Email This List To Me!</a>--}}
        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('success') }}</h4>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <h4>{{ Session::get('error') }}</h4>
            </div>
        @endif
        <a href="/account">Account</a> &raquo; Wishlist<br>
        <h1>Email Your Wish List</h1>
        <form method="post" action="/account/wishlist-email">
            {{csrf_field()}}
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email">
                </div>
            </div>

            <div class="clearfix"></div>
            <button type="submit">Submit</button>
        </form>

        <h2>Your Wish List</h2>
        @foreach($cs as $app)
            <a href="/app/{{$app->url_name}}"><h4>{{$app->name}}</h4></a>
            <p>{!! substr($app->description, 0, 150) !!}...</p>
        @endforeach
    </div>

@endsection


