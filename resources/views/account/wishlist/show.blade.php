@extends('layout')

@section('content')
    <style>
        .span4:nth-child(3n+1){
            clear:left;
        }
    </style>
    <div class="row">
        {{--<a href="#" class="pull-right clearfix">Email This List To Me!</a>--}}
        <a href="/account">Account</a> &raquo; Wishlist<br>
        <h1>My Wishlist</h1>
            @foreach($cs as $app)
                <div class="span4 col-xs-12 col-md-4 featured-app">
                    <?php $thumb = getThumbs($app->thumb_image_id) ?>
                    <img src="/files/images/versions/small/app_thumbs/{{$thumb->name.'-'.$thumb->id.'.'.$thumb->extension}}" alt="" class="pull-left"/>
                    <div class="app-info">
                        <div class="app-name app-info-item" style=''>
                            <a href="/app/{{$app->url_name}}">{{substr($app->name, 0, 30)}}
                                @if(strlen($app->name) > 30)
                                    ...
                                @endif
                            </a>
                        </div>
                        <div class="cat-name app-info-item">
                            <?php $i = 0; $catCount = count($app->categories)?>
                            @foreach($app->categories as $category)
                                <?php if(++$i > 2) {
                                    echo $catCount - 2 .' more';
                                    break;
                                }?>
                                <a href="/category/{{$category->url_name}}">{{$category->name}}</a>,

                            @endforeach
                        </div>
                        <div class="ages app-info-item">
                            Ages:
                            @foreach($app->ageRanges as $ageRange)
                                <a href="/app/agerange/{{$ageRange->url_name}}">{{$ageRange->range}}</a>,
                            @endforeach
                        </div>

                    </div>
                </div><!--end span4-->
            @endforeach
    </div>

@endsection


