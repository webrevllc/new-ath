@extends('layout')

@section('header')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

@endsection
@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-error">
            {{Session::get('error')}}
        </div>
    @endif

    <div class="form">
        <a href="/account">Account</a> &raquo; Edit
        <h1>Edit Your Information</h1>
        <form id="user-profile-edit-form" action="/account/edit/{{$profile->id}}" method="post" class="col-md-6">
            {{csrf_field()}}
            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <div class="form-group">
                <label for="UserProfile_first_name">First Name: <em>(Not Required)</em></label>
                <input name="UserProfile[first_name]" id="UserProfile_first_name" type="text" maxlength="255" value="{{$profile->first_name}}" class="form-control"/>                                </div>

            <div class="form-group">
                <label for="UserProfile_last_name">Last Name: <em>(Not Required)</em></label>
                <input name="UserProfile[last_name]" id="UserProfile_last_name" type="text" maxlength="255" value="{{$profile->last_name}}" class="form-control"/>                                </div>

            <div class="form-group">
                <label for="UserProfile_date_of_birth">Date Of Birth</label>
                <input class="datepicker form-control" name="UserProfile[date_of_birth]" id="datepicker" type="text" value="{{$profile->date_of_birth}}" />
            </div>

            <div class="form-group">
                <label for="UserProfile_country">Country</label>
                <input name="UserProfile[country]" id="UserProfile_country" type="text" maxlength="255" value="{{$profile->country}}" class="form-control"/>
            </div>

            <div class="form-group">
                <label for="UserProfile_gender">Gender</label>
                <input name="UserProfile[gender]" id="UserProfile_gender" type="text" maxlength="255" value="{{$profile->gender}}" class="form-control"/>
            </div>

            <div class="form-group">
                <label for="UserProfile_interested_for">Interested For</label>
                <select name="UserProfile[interested_for]" id="UserProfile_interested_for" class="form-control">
                    <option value="" selected="selected">-Select One-</option>
                    <option value="Children">Children</option>
                    <option value="Grandchildren">Grandchildren</option>
                    <option value="Other Relative">Other Relative</option>
                    <option value="Students">Students</option>
                    <option value="Self">Self</option>
                    <option value="Other">Other</option>
                </select>

            </div>

            <div class=" buttons">
                <input type="submit" name="yt0" value="Submit" class="pull-right"/>
            </div>

        </form>
    </div><!-- form -->


@endsection

@section('footer')
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker" ).datepicker({
                yearRange: "-100:+0",
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
@endsection