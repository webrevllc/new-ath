@extends('layout')

@section('content')

<title>My Account |App Treasure Hunter</title>

<div class="row reviews">

    <div class="col-md-3 ">
        <div class="left-column lt-yellow-box">
            <div class="details">
                <h2 style="margin-top:0px;">{{$user->profile->first_name .' '.$user->profile->last_name}}</h2>
                <a href="/account/edit">Edit</a> | <a href="/logout">Logout</a><br>

            </div><!--end .details-->
        </div>
        @if(auth()->user()->subscription('main'))
        <br>
        <div class="left-column lt-yellow-box">
            <h2 style="margin-top:0px;">Premium Area</h2>
            <ul>
                {{--<li><a href="#">Manage Payments</a></li>--}}
                <li><a href="/premium/invoices">View Payment History</a></li>
                <li><a href="/premium/cancel">Cancel Premium</a></li>
            </ul>
        </div>
        @else
            <br>
            <div class="left-column lt-yellow-box">
                <h2 style="margin-top:0px;"><a href="/premium/signup">Upgrade To Premium Today!</a></h2>
            </div>
        @endif
    </div>
    <div class="col-md-9">
        @if(Session::has('success'))
            <div class="alert alert-success span9">
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-error span9">
                {{Session::get('error')}}
            </div>
        @endif
        <h1 style="margin-top:0px;">My Account -
            @foreach($user->roles as $role)
                {{$role->label}}
            @endforeach
        </h1>

        <h2 style="margin-top:0px;">Account Options</h2>
        <p><a href="/account/profile">View/Edit Profile</a></p>
        <p><a href="/account/password">Change Password</a></p>

        @can('manage_site')
            <h2 style="margin-top:0px;">Admin Options</h2>
            {{--<p><a href="/account/site-config">Site Config</a></p>--}}
            <p><a href="/account/dashboard">Dashboard</a></p>
        @endcan

        @can('review_apps')
            <h2 style="margin-top:0px;">Reviewer Options</h2>
            <p><a href="{{route('admin.apps.create')}}">Add Review</a></p>
            <p><a href="{{route('admin.myreviews')}}">Your Reviews</a></p>
        @endcan

        {{--@can('view_premium_area')--}}
            {{--<h2 style="margin-top:0px;">Premium Options</h2>--}}
            {{--<p><a href="/premium/feature1">Custom Premium Feature</a></p>--}}
            {{--<p><a href="/premium/feature2">Awesome Cool Premium Feature</a></p>--}}
        {{--@endcan--}}

        @if(auth()->user()->subscription('main'))
            <h2 style="margin-top:0px;">Premium Options</h2>
            <p><a href="/premium/feature1">Custom Premium Feature</a></p>
            <p><a href="/premium/feature2">Awesome Cool Premium Feature</a></p>
        @endif

        <h2 style="margin-top:0px;">Students</h2>
        <ul>
            @if(count($user->students) > 0)
                @foreach($user->students as $student)
                    <li><a href="{{route('students.show', [$student->id])}}">{{$student->first_name}} {{$student->last_name}}</a></li>
                @endforeach
            @endif

            <li><a href="{{route('students.create')}}"><em> Add Student</em></a></li>
        </ul>
        <p>
            <a href="/account/wishlist">View Wishlist</a><br>
            <a href="/account/wishlist-email">Email Wishlist </a>
        </p>
    </div>
    <!--end .span9-->

</div><!--end .row-->
@endsection