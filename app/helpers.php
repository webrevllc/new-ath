<?php


use App\Profile;
use App\Student;
use App\User;
use Carbon\Carbon;

function getThumbs($id){
    $thumb = \App\Screenshot::find($id);
    return $thumb;
}

function getScreenshots($id){
    $thumb = \App\Screenshot::find($id);
    return $thumb;
}

function getStudentAge(Student $student){

    $now = Carbon::now();
    $dob = Carbon::createFromDate($student->birth_year, $student->birth_month, $student->birth_day);

    return $dob->diffInYears($now);

}

function getUserAge(Profile $profile){

    $now = Carbon::now();
    $dob = Carbon::createFromTimestamp(strtotime($profile->date_of_birth));

    return $dob->diffInYears($now);

}