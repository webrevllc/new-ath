<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
    protected $fillable = ['name', 'url_name', 'is_deleted'];

    public function apps()
    {
        return $this->belongsToMany(App::class, 'app_category', 'category_id');
    }

    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }
}
