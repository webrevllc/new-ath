<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    protected $table = 'review_video';

    public function apps()
    {
        return $this->belongsTo(App::class);
    }
}
