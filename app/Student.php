<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    protected $fillable = [
        'first_name',
        'last_name',
        'birth_month',
        'birth_day',
        'birth_year',
        'subjects_of_interest',
        'subjects_of_difficulty',
        'learning_level',
        'comments',
    ];

    public function apps()
    {
        return $this->belongsToMany(App::class, 'student_app')->withPivot('is_owned', 'comments', 'proficiency');
    }

    public function parent()
    {
        return $this->belongsTo(User::class);
    }

    public function ownsApp($id)
    {
        return $this->apps->contains('id', $id);
    }
}
