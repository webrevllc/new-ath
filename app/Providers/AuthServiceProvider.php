<?php

namespace App\Providers;

use App\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(\Illuminate\Contracts\Auth\Access\Gate $gate)
    {
        $gate->define('owns-student', function($user, $student){
            return $student->parent_user_id == $user->id || $user->hasRole('admin');
        });

        $this->registerPolicies($gate);

        foreach($this->getPermissions() as $permission){
            $gate->define($permission->name, function ($user) use($permission){
                return $user->hasRole($permission->roles);
            });
        }
    }

    public function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}
