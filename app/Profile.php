<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'user_profile';
    protected $fillable = [
        'first_name', 'last_name', 'date_of_birth', 'country', 'gender', 'interested_for'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
