<?php

namespace App\Http\Controllers;

use App\LearningLevel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LevelController extends Controller
{

    public function index()
    {
        $levels = LearningLevel::get();
        return view('admin.levels.index', compact('levels'));
    }

    public function create()
    {
        return view('admin.levels.create');
    }

    public function edit($id)
    {
        $level = LearningLevel::find($id);
        return view('admin.levels.edit', compact('level'));
    }

    public function store(Request $request)
    {
        $level = new LearningLevel;
        $level->fill($request->Level);
        $level->save();
        return redirect(route('admin.learninglevels.index'))->withInput()->with('success', $level->level.' Successfully Created!');
    }

    public function update($id, Request $request)
    {
        $level = LearningLevel::find($id);
        $level->fill($request->Level);
        $level->save();
        return redirect(route('admin.learninglevels.index'))->withInput()->with('success', $level->level.' Successfully Updated!');
    }

    public function deleteLevel($id)
    {
        $level = LearningLevel::find($id);
        $level->is_deleted = true;
        $level->save();
        return redirect(route('admin.learninglevels.index'))->withInput()->with('success', $level->level.' Successfully Deleted!');
    }
}
