<?php

namespace App\Http\Controllers;

use App\App;
use App\Wishlist;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;

class WishlistController extends Controller
{
    public function assignApp(App $app)
    {
        $user = auth()->user();

        if(! Wishlist::where('user_id', $user->id)->where('app_id', $app->id)->exists()){
            $wishlist = new Wishlist();
            $wishlist->user_id = $user->id;;
            $wishlist->app_id = $app->id;
            $wishlist->save();
            return redirect()->back()->withInput()->with('success', 'App Successfully Added To Wishlist!');
        }else{
            return redirect()->back()->withInput()->with('error', 'Sorry, you already have that app saved to your list.');
        }

    }

    public function viewWishlist()
    {
        $apps = auth()->user()->wishlistApps()->get();
        $theApps = [];
        foreach ($apps as $app){
            $theApps[] = App::find($app->app_id);
        }
        $cs = new Collection($theApps);
        return view('account.wishlist.show', compact('cs'));
    }

    public function showEmailPage()
    {
        $apps = auth()->user()->wishlistApps()->get();
        $theApps = [];
        foreach ($apps as $app){
            $theApps[] = App::find($app->app_id);
        }
        $cs = new Collection($theApps);
        return view('account.wishlist.email', compact('cs'));
    }

    public function sendEmail(Request $request)
    {
        $user = auth()->user();
        $apps = auth()->user()->wishlistApps()->get();
        $theApps = [];
        foreach ($apps as $app){
            $theApps[] = App::find($app->app_id);
        }
        $cs = new Collection($theApps);
        Mail::send('email.wishlist', ['apps' => $cs], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Your Wishlist!');
        });

        return redirect()->back()->withInput()->with('success', 'Your wishlist has been emailed!');
    }

}
