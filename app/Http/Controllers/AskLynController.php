<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;

class AskLynController extends Controller
{
    public function admin_email()
    {
        return DB::select("select * from site_config")[0]->site_email;
    }

    public function getLyn()
    {
        return view('ask-lyn');
    }

    public function postLyn(Request $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'question' => $request->question
        ];
        Mail::send('email.contactlyn',['data' => $data], function ($message) {
            $message->from('website@couponcontrols.com', 'AppTreasureHunter');
            $message->to('srosenthal82@gmail.com');
            $message->subject('New Ask Lyn Form Posted!');
//            $message->to($this->admin_email());
        });

        return redirect()->back()->withInput()->with('success', 'Your request has been submitted!');
    }

    public function contact(Request $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'question' => $request->question
        ];
        Mail::send('email.contactlyn',['data' => $data], function ($message) {
            $message->from('website@apptreasurehunter.com', 'AppTreasureHunter');
            $message->to('srosenthal82@gmail.com');
            $message->subject('New Contact Us Form Posted!');
//            $message->to($this->admin_email());
        });

        return redirect()->back()->withInput()->with('success', 'Your request has been submitted!');
    }
}
