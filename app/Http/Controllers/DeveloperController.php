<?php

namespace App\Http\Controllers;

use App\Developer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DeveloperController extends Controller
{

    public function index()
    {
        $developers = Developer::orderBy('name')->get();
        return view('admin.developers.index', compact('developers'));
    }

    public function create()
    {
        return view('admin.developers.create');
    }

    public function store(Request $request)
    {
        $developer = new Developer;
        $developer->fill($request->Developer);
        $developer->save();
        return redirect()->back()->withInput()->with('success', 'Developer Successfully Created!');
    }

    public function edit($id)
    {
        $developer = Developer::find($id);
        return view('admin.developers.edit', compact('developer'));
    }

    public function update($id, Request $request)
    {
        $developer = Developer::find($id);
        $developer->fill($request->Developer);
        $developer->save();
        return redirect()->back()->withInput()->with('success', 'Developer Successfully Updated!');
    }

    public function delete($id)
    {
        $developer = Developer::find($id);
        $developer->is_deleted = true;
        $developer->save();
        return redirect()->back()->withInput()->with('success', 'Developer Successfully Deleted!');
    }
}
