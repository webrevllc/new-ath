<?php

namespace App\Http\Controllers;

use App\AgeRange;
use App\App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AgeRangeController extends Controller
{
    public function show($id)
    {
        if($id == "all"){
            $apps = App::paginate(20);
        }else{
            $apps = App::with(['ageRanges' => function($query) use($id){
                $query->where('id', $id);
            }])->with('categories')->paginate(20);
        }

        return view('search-results', compact('apps'));
    }

    public function get()
    {
        $data = [
            'ages' => AgeRange::get()
        ];
        return $data;
    }

    public function index()
    {
        $ages = AgeRange::get();
        return view('admin.ages.index', compact('ages'));
    }

    public function create()
    {
        return view('admin.ages.create');
    }

    public function edit($id)
    {
        $range = AgeRange::find($id);
        return view('admin.ages.edit', compact('range'));
    }

    public function store(Request $request)
    {
        $range = new AgeRange;
        $range->fill($request->Range);
        $range->save();
        return redirect(route('admin.ageranges.index'))->withInput()->with('success', $range->range.' Successfully Created!');
    }

    public function update($id, Request $request)
    {
        $range = AgeRange::find($id);
        $range->fill($request->Range);
        $range->save();
        return redirect(route('admin.ageranges.index'))->withInput()->with('success', $range->range.' Successfully Updated!');
    }

    public function deleteRange($id)
    {
        $range = AgeRange::with('apps')->find($id);
        if(count($range->apps) > 0){
            return redirect(route('admin.ageranges.index'))->withInput()->with('error', 'Sorry, that age range has apps assigned to it.');
        }else{
            $range->is_deleted = true;
            $range->save();
            return redirect(route('admin.ageranges.index'))->withInput()->with('success', $range->level.' Successfully Deleted!');
        }

    }
}
