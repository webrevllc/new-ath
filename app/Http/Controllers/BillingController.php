<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BillingController extends Controller
{
    public function createSubscription(Request $request)
    {
        $user = auth()->user();
        $user->newSubscription('main', 'monthly_plan')->create($request->payment_method_nonce);
        return redirect('/premium/feature1');
    }

    public function getInvoices()
    {
        $user = auth()->user();
        $invoices = $user->invoices();
        return view('premium.invoices', compact('invoices'));
    }

    public function cancelSubscription()
    {
        $user = auth()->user();
        if($user->subscription('main')){
            auth()->user()->subscription('main')->cancel();
            return redirect()->back()->withInput()->with('success', 'Your subscription has been cancelled, access to premium features will terminate at the end of your billing cycle.');
        }else{
            return redirect()->back()->withInput()->with('error', 'You are not currently subscribed to that plan.');
        }
    }
}
