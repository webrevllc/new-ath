<?php

namespace App\Http\Controllers;

use App\AgeRange;
use App\App;
use App\AppFilters;
use App\Category;
use App\Developer;

use App\Reviewer;
use App\Screenshot;
use App\Subcategory;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class AppController extends Controller
{
    public function getFeatured()
    {
        $apps = App::with('ageRanges', 'categories', 'subcategories', 'screenshots')->where('is_featured', true)->where('is_approved', true)->where('is_deleted', false)->get();
        return view('featured', compact('apps'));
    }

    public function getNewest()
    {
        $apps = App::with('ageRanges', 'categories', 'subcategories', 'screenshots')->orderBy('created_at', 'DESC')->where('is_approved', true)->where('is_deleted', false)->paginate(12);
        return view('newest', compact('apps'));
    }

    public function show($app)
    {
        try{
            $theApp = App::with('ageRanges', 'categories', 'screenshots', 'subcategories', 'developer', 'reviewer')->where('url_name', $app)->where('is_approved', true)->firstOrFail();
//            dd($theApp);
            return view('show', compact('theApp'));
        }catch(ModelNotFoundException $m){
            return view('not-found');
        }

    }

    public function getDeveloper($name)
    {
        $developer = Developer::where('url_name', $name)->first();
        $apps = App::where('developer_id', $developer->id)->where('is_approved', true)->get();
        return view('developer', compact('apps', 'developer'));
    }

    //////ADMIN APPS CRUD FUNCTIONS

    public function index()
    {
        $apps = App::orderBy('created_at', 'DESC')->get();
        return view('admin.apps.apps', compact('apps'));
    }

    public function create()
    {
        $developers = Developer::orderBy('name', 'asc')->get();
        $categories = Category::get();
        $ages = AgeRange::get();
        return view('admin.apps.create', compact('developers', 'categories', 'ages'));
    }

    public function store(Request $request)
    {
        $app = new App;
        $app->fill($request->App);
        if($request->has('App[\'is_featured\']')){
            $request->App['is_featured'] == 'true' ? $app->is_featured = true : $app->is_featured = false;
            $request->App['is_approved'] == 'true' ? $app->is_approved = true : $app->is_approved = false;
            $request->App['is_deleted'] == 'true' ? $app->is_deleted = true : $app->is_deleted = false;
        }

        $app->created_by = auth()->user()->id;
        $app->save();

        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $this->makeThumbnail($file, $app);
        }

        $app->categories()->detach();
        if($request->has('AppCategory')) {
            foreach ($request->AppCategory as $k => $v) {
                $app->categories()->attach($k);
            }
        }

        $app->subcategories()->detach();
        if($request->has('AppSubcategory')){
            foreach($request->AppSubcategory as $k => $v){
                $app->subcategories()->attach($k);
            }
        }

        $app->ageranges()->detach();
        if($request->has('AppAgerange')){
            foreach($request->AppAgerange as $k => $v){
                $app->ageranges()->attach($k);
            }
        }
        if(auth()->user()->hasRole('admin')){
            return redirect(route('admin.apps.index'))->withInput()->with('success', 'App Successfully Created!');
        }else{
            return redirect(route('admin.myreviews'))->withInput()->with('success', 'Review Successfully Created!');
        }

    }

    public function edit($id)
    {
        $app = App::with('categories', 'subcategories', 'ageRanges', 'reviewer')->find($id);

        $developers = Developer::orderBy('name', 'asc')->get();
        $categories = Category::with('subcategories')->orderBy('name', 'asc')->get();
        $subs = Subcategory::with('category')->orderBy('name', 'asc')->get();
        $ages = AgeRange::get();
        return view('admin.apps.edit', compact('app', 'developers', 'categories', 'subs', 'ages'));
    }

    public function update($id, Request $request)
    {
        $app = App::find($id);
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $this->makeThumbnail($file, $app);
        }
        $app->fill($request->App);
        if(array_key_exists('is_featured', $request->App)){
            $request->App['is_featured'] == 'true' ? $app->is_featured = true : $app->is_featured = false;
        }
        if(array_key_exists('is_approved', $request->App)){
            $request->App['is_approved'] == 'true' ? $app->is_approved = true : $app->is_approved = false;
        }
        if(array_key_exists('is_deleted', $request->App)){
            $request->App['is_deleted'] == 'true' ? $app->is_deleted = true : $app->is_deleted = false;
        }



        $app->categories()->detach();
        if($request->has('AppCategory')) {
            foreach ($request->AppCategory as $k => $v) {
                $app->categories()->attach($k);
            }
        }

        $app->subcategories()->detach();
        if($request->has('AppSubcategory')){
            foreach($request->AppSubcategory as $k => $v){
                $app->subcategories()->attach($k);
            }
        }

        $app->ageranges()->detach();
        if($request->has('AppAgerange')){
            foreach($request->AppAgerange as $k => $v){
                $app->ageranges()->attach($k);
            }
        }


        if($request->ReviewVideo['url'] != ''){
            $review = new Reviewer();
            $review->title = $request->ReviewVideo['title'];
            $review->url = $request->ReviewVideo['url'];
            $app->reviewer()->save($review);
        }
        $app->save();

        if(auth()->user()->hasRole('admin')){
            return redirect(route('admin.apps.index'))->withInput()->with('success', 'App Successfully Updated!');
        }else{
            return redirect(route('admin.myreviews'))->withInput()->with('success', 'Review Successfully Updated!');
        }

    }

    public function approve($id)
    {
        $app = App::find($id);
        $app->is_approved = true;
        $app->save();

        //FIRE APP APPROVED EVENT
        return redirect()->back();
    }

    public function makeThumbnail($file, App $app)
    {
        $screenshot = new Screenshot;
        $screenshot->name  = substr($file->getClientOriginalName(), 0, -4);
        $screenshot->path = 'app/thumbs';
        $screenshot->extension = substr($file->getClientOriginalName(),strpos($file->getClientOriginalName(), ".") + 1);
        $screenshot->filename = $file->getClientOriginalName();
        $screenshot->byteSize = $file->getClientSize();
        $screenshot->mimeType = $file->getMimeType();
        $screenshot->is_thumb = true;
        $screenshot->save();

        $name = $screenshot->name.'-'.$screenshot->id.'.'.$screenshot->extension;

        $file->move('files/images/versions/small/app_thumbs', $name);
        $app->thumb_image_id = $screenshot->id;
        $app->save();

    }

    public function getTestimonial(App $app)
    {
        return view('admin.apps.testimonials', compact('app'));
    }

    public function screenshots($id)
    {
        $app = App::with('screenshots')->find($id);
        return view('admin.apps.screenshots', compact('app'));
    }

    public function destroy($id)
    {
        $app = App::find($id);
        $app->is_deleted = true;
        $app->save();
        return redirect()->back();
    }

    public function cancelApproval($id)
    {
        $app = App::find($id);
        $app->is_approved = false;
        $app->save();
        return redirect()->back();
    }

    public function search(AppFilters $filters, Request $request)
    {
        $filteredApps =  App::where('is_approved', true)->orderBy('name', 'ASC')->filter($filters);
        $total = count($filteredApps->get());
        if(array_key_exists('results', $request->all())) {
            $amount = $request->results == 'all' ? 9999 : $request->results;
        }else{
            $amount = 25;
        }
        $apps = $filteredApps->paginate($amount);
        return view('search-results', compact('apps', 'total'));
    }

    public function myReviews()
    {
        $apps = App::where('created_by', auth()->user()->id)->get();
        return view('admin.apps.myapps', compact('apps'));
    }

}
