<?php

namespace App\Http\Controllers\Auth;

use App\Profile;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Laravel\Socialite\Facades\Socialite;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
//            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'username' => $data['email'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $profile = new Profile;
        $profile->fill($data['UserProfile']);
        $user->profile()->save($profile);

        $user->assignRole(3);
        return $user;
    }

    public function redirectToProvider()
    {
        return Socialite::with('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::with('facebook')->user();
        $token = $user->token;
        $email = $user->getEmail();

        try{
            $existingUser = User::where('email', $email)->firstOrFail();
            Auth::loginUsingid($existingUser->id);
            return redirect('/account');
        }catch(ModelNotFoundException $m){
            $u = new \App\User;
            $u->email = $user->getEmail();
            $u->username = $u->email;
            $u->save();
            $profile = new Profile;
            $profile->first_name = $user->getName();
            $u->profile()->save($profile);

            $u->assignRole(3);
            Auth::loginUsingid($u->id);
            return redirect('/account');
        }
    }
}
