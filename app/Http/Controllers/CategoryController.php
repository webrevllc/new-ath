<?php

namespace App\Http\Controllers;

use App\App;
use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function get()
    {
        $data = [
            'categories' => Category::get()
        ];
        return $data;
    }

    public function getCategory($name)
    {
        $category = Category::with('apps')->where('url_name', $name)->first();
        $apps = $category->apps;
        return view('category-results', compact('apps', 'category'));
    }

    public function getSubCategories($name)
    {
        $category = Subcategory::with('apps')->where('url_name', $name)->first();
        $apps = $category->apps;
        return view('category-results', compact('apps', 'category'));
    }

    public function getSubs($id)
    {
        $data = [
            'subcategories' => Subcategory::where('category_id', $id)->get()
        ];
        return $data;
    }


    //////ADMIN APPS CRUD FUNCTIONS
    public function index()
    {
        $categories = Category::with('subcategories')->where('is_deleted', '!=', true)->orderBy('name')->get();
        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $category = new Category;
        $category->fill($request->Category);
        $category->save();
        return redirect(route('admin.categories.index'))->withInput()->with('success', $category->name.' Successfully Created!');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {
//        dd($request->all());
        $category = Category::find($id);
        $category->fill($request->Category);
        $category->save();
        return redirect(route('admin.categories.index'))->withInput()->with('success', $category->name.' Successfully Updated!');
    }

    public function deleteCategory($id)
    {
        return redirect()->back()->withInput()->with('error', 'Sorry, that Subcategory has apps assigned to it, please remove all apps in this subcategory before attempting to delete it.');

        $category = Category::with('apps')->find($id);
        if(count($category->apps) > 0){
        }else{
            $category->is_deleted = true;
            $category->save();
            return redirect(route('admin.categories.index'))->withInput()->with('success', $category->name.' Successfully Updated!');
        }

    }

    public function hello()
    {
        return redirect('/admin/ok')->withInput()->with('error', 'Sorry, that Subcategory has apps assigned to it, please remove all apps in this subcategory before attempting to delete it.');
    }

    public function addSub($id)
    {
        $category = Category::find($id);
        return view ('admin.subcategories.create', compact('category'));
    }

    public function getCats()
    {
        $cats = Category::where('is_deleted', '!=', true)->get();
        return $cats;
    }

    public function getTheSubs(Request $request)
    {
        $cats = $request->cats;
        $subs = [];
        foreach($cats as $c){
            $category = Category::with('subcategories')->find($c);

            if(count($category->subcategories) > 0){
                $subs[] = $category;
            }
        }
        return json_encode($subs);
    }
}
