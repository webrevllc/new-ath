<?php

namespace App\Http\Controllers;

use App\App;
use App\Student;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function get()
    {
        if(auth()->check()){
            $data = [
                "students" => auth()->user()->students
            ];
            return $data;
        }
        return false;
    }

    public function show($id)
    {
        try{
            $student = Student::findOrFail($id);
            $this->authorize('owns-student', $student);
            return view('account.students.show', compact('student'));
        }catch(ModelNotFoundException $m){
            abort(404);
        }

    }

    public function studentApps($id)
    {
        $student = Student::with('apps')->find($id);
        $data = [
            'apps' => $student->apps
        ];
        return $data;
    }

    public function updateExistingStudentApp($studentID, $appID, Request $request)
    {
        if(Student::find($studentID)->apps()->updateExistingPivot($appID, $request->data)){
            return 'success';
        }

        return 'error';

    }

    public function edit($id)
    {

        $student = Student::find($id);
        return view('account.students.edit', compact('student'));
    }

    public function update($id, Request $request)
    {
        $student = Student::find($id);
        $student->fill($request->Student);
        $student->save();
        return redirect('/account')->withInput()->with('success', 'Student successfully updated');
    }

    public function create()
    {
        return view('account.students.create');
    }

    public function store(Request $request)
    {
        $student = new Student();
        $student->fill($request->Student);
        $student->parent_user_id = auth()->user()->id;

        $student->save();

        return redirect('/account')->withInput()->with('success', 'Student successfully created');
    }

    public function addAppToStudent($appId, $studentId)
    {
        $student = Student::find($studentId);
        try{
            $student->apps()->attach($appId);
            return redirect()->back()->withInput()->with('success', 'App successfully added to child\'s profile!');
        }catch(QueryException $q){
            return redirect()->back()->withInput()->with('error', 'App already added to that student\'s profile');
        }
    }

    public function removeAppFromStudent($appId, $studentId)
    {
        $student = Student::find($studentId);
        try{
            $student->apps()->detach($appId);
            return redirect()->back()->withInput()->with('success', 'App successfully removed from that child\'s profile!');
        }catch(QueryException $q){
            return redirect()->back()->withInput()->with('error', 'App already removed from that student\'s profile');
        }
    }

    public function checkApp($appId, $studentId)
    {
        $student = Student::find($studentId);
//        return $student->apps;
        if($student->ownsApp($appId)){
            return "true";
        }else{
            return "false";
        }
    }

    public function destroy(Student $student)
    {
        $student->delete();
        return redirect('/account')->withInput()->with('success', "Student successfully deleted");
    }
}
