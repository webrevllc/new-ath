<?php

namespace App\Http\Controllers;

use App\AgeRange;
use App\App;
use App\Category;
use App\Developer;
use App\LearningLevel;
use App\Reviewer;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function reviews()
    {
        $reviews = App::orderBy('id', 'asc')->get();
        return view('admin.reviews', compact('reviews'));
    }
    public function videos()
    {
        return view('admin.videos');
    }
}
