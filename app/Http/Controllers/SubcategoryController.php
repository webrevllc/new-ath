<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SubcategoryController extends Controller
{

    public function edit($id)
    {
        $category = Subcategory::find($id);
        return view('admin.subcategories.edit', compact('category'));
    }

    public function deleteSubcategory($id)
    {
        $category = Subcategory::with('apps')->find($id);
        if(count($category->apps) > 0){
            return redirect(route('admin.categories.index'))->withInput()->with('error', 'Sorry, that Subcategory has apps assigned to it, please remove all apps in this subcategory before attempting to delete it.');
        }else{
            $category->is_deleted = true;
            $category->save();
            return redirect(route('categories.index'))->withInput()->with('success', $category->name.' Successfully Deleted!');
        }

    }

    public function make($id, Request $request)
    {
        $subcategory = new Subcategory;
        $subcategory->fill($request->Category);
        $subcategory->category_id = $id;
        $subcategory->save();
        return redirect(route('admin.categories.index'))->withInput()->with('success', $subcategory->name.' Successfully Added!');
    }

    public function update($id, Request $request)
    {
        $category = Subcategory::find($id);
        $category->fill($request->Category);
        $category->save();
        return redirect(route('admin.categories.index'))->withInput()->with('success', $category->name.' Successfully Updated!');
    }
}
