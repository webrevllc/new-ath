<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function index()
    {
        $newsletters = Newsletter::get();
        return view('newsletters', compact('newsletters'));
    }

    public function show(Newsletter $newsletter)
    {
//        dd(Newsletter::find($id));
        return view ('newsletters.show', compact('newsletter'));
    }
}
