<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Student;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{

    public function home()
    {
//        $existingUser = User::where('email', 'srosenthal82@gmail.com')->firstOrFail();
//        Auth::loginUsingid($existingUser->id);
//        dd(auth()->user());
        $user = User::with('profile', 'students', 'roles')->find(auth()->user()->id);

        return view('account.home', compact('user'));
    }
    public function edit()
    {
        $profile = auth()->user()->profile;
        return view('account.edit', compact('profile'));
    }

    public function postEdit($id,Request $request)
    {
        $profile = Profile::find($id);
        $newData = $request->UserProfile;
        $profile->first_name = $newData['first_name'];
        $profile->last_name = $newData['last_name'];
        $profile->date_of_birth = date('Y-m-d', strtotime($newData['date_of_birth']));
        $profile->country = $newData['country'];
        $profile->gender = $newData['gender'];
        $profile->interested_for = $newData['interested_for'];
        $profile->save();
        return redirect('/account')->withInput()->with('success', 'Profile successfully updated');
    }

    public function profile()
    {
        $profile = auth()->user()->profile;
        $students = Student::where('parent_user_id', auth()->user()->id)->with('apps')->get();
        return view('account.profile', compact('profile', 'students'));
    }

    public function password()
    {
        return view('account.password');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6'
        ]);
        $user = auth()->user();
        if($request->password == $request->password_confirmation && $request->password !== ''){
            $user->password = bcrypt($request->password);
            $user->save();
            return redirect('/account')->withInput()->with('success', 'Password successfully changed');
        }else{
            return redirect()->back()->withInput()->with('error', 'Password confirmation doesn\'t match');
        }
    }

    public function getDashboard()
    {

        return view('account.dashboard');
    }
}
