<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function index()
    {
        $users = User::with('profile', 'roles')->get();
        return view('admin.users.index', compact('users'));
    }

    public function show($id)
    {
        $user = User::with('profile', 'students')->find($id);
        return view('admin.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::with('profile', 'roles')->find($id);
        $roles = Role::get();
        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update($id, Request $request)
    {
        $user = User::with('roles', 'profile')->find($id);
        $user->fill($request->User);
        $user->save();

        $user->profile->fill($request->UserProfile);
        $user->profile->save();

        $user->roles()->detach();
        $user->assignRole($request->UserRole['role_id']);

        return redirect()->back()->withInput()->with('success', 'User successfully updated!');
    }

    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->is_deleted = true;
//        if($user->is_deleted){
//            $user->is_deleted = false;
//        }else{
//            $user->is_deleted = true;
//        }
        $user->save();
//                dd($user->is_deleted);

        return redirect()->back()->withInput()->with('success', 'User successfully updated!');
    }

    public function resendVerification(Request $request)
    {
        $user = User::find($request->id);
        $confirmation_code = str_random(30);
        $user->verification_code = $confirmation_code;
        $user->save();

        Mail::send('email.verify', ['code' => $confirmation_code], function($message) use($user){
            $message->to($user->email)
                ->subject('Verify your email address');
        });

        return "Email Sent";
    }

    public function verify($code)
    {
        $user = User::where('verification_code', $code)->firstOrFail();
        $user->is_verified = true;
        $user->save();

        return redirect('/account');
    }
}
