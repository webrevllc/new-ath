<?php

namespace App\Http\Middleware;

use Closure;

class Premium
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            if(! auth()->user()->can('view_premium_area')){
                return redirect('/not-paid');
            }
        }else{
            return redirect('/login');
        }
        return $next($request);
    }
}
