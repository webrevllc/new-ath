<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    protected $table = "app";

    protected $fillable = [
        'name',
        'url_name',
        'description',
        'developer_id',
        'features',
        'accolades',
        'price',
        'itunes_url',
        'is_featured',
        'star_rating',
//        'thumb_image_id',
        'is_approved',
        'approved_by',
        'approved_on',
        'is_deleted',
        'wp_post_id',
        'youtube_url',
        'review_url',
    ];

    public function ageRanges()
    {
        return $this->belongsToMany(AgeRange::class, 'app_agerange', '', 'agerange_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'app_category', 'app_id');
    }

    public function subcategories()
    {
        return $this->belongsToMany(Subcategory::class, 'app_subcategory');
    }

    public function screenshots()
    {
        return $this->belongsToMany(Screenshot::class, 'app_screenshot', '', 'screenshot_image_id');
    }

    public function developer()
    {
        return $this->belongsTo(Developer::class);
    }

    public function reviewer()
    {
        return $this->hasOne(Reviewer::class);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

}
