<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 5/3/2016
 * Time: 9:47 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilter
{

    protected $request;
    protected $builder;

    /**
     * QueryFilter constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        if(array_key_exists('results', $this->request->all())){
            $this->filters()['page'] = "1";
        }
        foreach ($this->filters() as $name => $value) {
            if (! method_exists($this, $name)) {
                continue;
            }
            if (strlen($value)) {
                $this->$name($value);
            } else {
                $this->$name();
            }
        }
        return $this->builder;
    }

    public function filters()
    {
        if($this->request['results'] == "all"){
            unset($this->request['page']);
        }
        return $this->request->all();
    }
}