<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Billable, Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'is_deleted', 'verification_code'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($role)
    {
        if(is_string($role)){
            return $this->roles->contains('name', $role);
        }

//        return !! $role->intersect($this->roles)->count();

        foreach($role as $r){
            if($this->hasRole($r->name)){
                return true;
            }
        }
        return false;
    }

    public function assignRole($id)
    {
        return $this->roles()->save(
            Role::find($id)
        );
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'parent_user_id');
    }

    public function wishlistApps()
    {
        return $this->hasMany(Wishlist::class);
    }
}
