<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $table = 'developer';
    protected $fillable = ['name', 'url_name', 'is_deleted'];

    public function apps()
    {
        return $this->belongsToMany(App::class);
    }
}
