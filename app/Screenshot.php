<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Screenshot extends Model
{
    protected $table = 'image';
    public function app()
    {
        return $this->belongsToMany(App::class, 'image', 'screenshot_image_id');
    }

    public function is_thumb()
    {
        return $this->is_thumb;
    }
}
