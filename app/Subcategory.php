<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = "subcategory";
    protected $fillable = ['name', 'url_name', 'is_deleted'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function apps()
    {
        return $this->belongsToMany(App::class, 'app_subcategory', 'subcategory_id');
    }
}
