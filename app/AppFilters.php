<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 5/3/2016
 * Time: 9:39 AM
 */

namespace App;


class AppFilters extends QueryFilter
{

    public function term($term = '')
    {
        if($term != ''){
            return $this->builder->where('name', 'LIKE', "%$term%")
                ->orWhere('description', 'LIKE', "%$term%");
        }
    }

    public function range($agerange = '')
    {
        return $this->builder->whereHas('ageRanges', function($query) use($agerange){
                if($agerange == '4' || $agerange == ''){
                    $query->where('agerange_id', '>', 0);
                }else{
                    $query->where('agerange_id', $agerange);
                }
            });
    }

    public function category($category = '')
    {
        if($category != '') {
            return $this->builder->whereHas('categories', function ($query) use ($category) {
                $query->where('category_id', $category);
            });
        }else{

        }
    }

    public function subcategory($subcategory = '')
    {
        if($subcategory != '') {
            return $this->builder->whereHas('subcategories', function ($query) use ($subcategory) {
                $query->where('subcategory_id', $subcategory);
            });
        }else{
            
        }
    }



//    public function results($results = '')
//    {
//        if($results != ''){
//            return $this->builder->where('name', 'like', '%'. $term. '%')
//                ->orWhere('description', 'like', '%'.$term.'%');
//        }
//    }
}