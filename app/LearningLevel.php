<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningLevel extends Model
{
    protected $table = 'learning_level';
    protected $fillable = ['level', 'url_name', 'is_deleted'];
}
