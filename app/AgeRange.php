<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgeRange extends Model
{
    protected $table = "age_range";
    protected $fillable = ['range', 'url_name', 'is_deleted'];

    public function apps()
    {
        return $this->belongsToMany(App::class, 'app_agerange', 'agerange_id');
    }
}
