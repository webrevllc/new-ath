<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    public function apps()
    {
        return $this->hasMany(App::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
