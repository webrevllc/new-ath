// set up datepicker
$(document).ready(function(){
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true, 
        yearRange: "-70:+0"
        
    });
});

// Set up youtube modal box
$(".youtube").click(function(e){
    e.preventDefault() ;
    thislink = $(this).attr("href");
    $.colorbox({
        href: thislink,
        iframe: true,
        width: "80%",
        height: "80%"
    })
});

// Search function
$(document).ready(function(){
    $("#search-form #category").change(function(){
        postdata = {};
        postdata.category_id = $(this).val();
        $.ajax({
            url:    "/subcategory/getselectbox/",
            method: "post",
            data:   postdata,
            success:function(response){
                $("#subcategory-select-wrapper").html(response);
            }
        });
    });
});  

// Wishlist Functions
$(document).ready(function(){
    setUpWishlistadd();
    setupWishlistRemove();
    function setUpWishlistadd(){
       $(".wishlist-add").click(function(){
            app_id = $(this).parent().attr("data-app_id");
            element = $(this).parent();

            $.ajax({
                url: "/wishlist/add/app_id/" + app_id,
                method: "post",
                success: function(response){
                    console.log(response);
                    $(element).replaceWith(response);

                    setupWishlistRemove();
                }
            });
        }); 
    }
    function setupWishlistRemove()
    {
        $(".wishlist-remove").click(function(){
            app_id = $(this).parent().attr("data-app_id");
            element = $(this).parent();
            $.ajax({
                url: "/wishlist/remove/app_id/" + app_id,
                method: "post",
                success: function(response){
                    $(element).replaceWith(response);
                    setUpWishlistadd();
                }
            });
        });
    }
});

// Child Profile Add App
$(document).ready(function(){
    setUpStudentAppAdd();
    setUpStudentAppRemove();
    function setUpStudentAppAdd(){
        $(".which-student").change(function(){
            student_id = $(".which-student",$(this).parent()).val();
            app_id = $(this).parent().attr("data-app_id");
            parent_div = $(this).parent();
            postdata = {};
            postdata.student_id = student_id;
            postdata.app_id = app_id;
            $.ajax({
                url: "/student/changestudent",
                data: postdata,
                method: "post",
                success: function(response){
                    $(parent_div).replaceWith(response);
                    setUpStudentAppAdd();
                    setUpStudentAppRemove();
                }
            });
        });
        
       $(".student-app-add").click(function(){
           $(".student-app-remove").unbind("click");
           $(".student-app-add").unbind("click");
            app_id = $(this).parent().attr("data-app_id");
            student_id = $(".which-student",$(this).parent()).val();
            ele = $(this).parent();
            postdata = {};
            postdata.app_id = app_id;
            postdata.student_id = student_id;
            $.ajax({
                url: "/student/addapp",
                data: postdata,
                method: "post",
                success: function(response){
                    $(ele).replaceWith(response);
                    setUpStudentAppAdd();
                    setUpStudentAppRemove();
                }
            });
        }); 
    }
    
    function setUpStudentAppRemove(){
       $(".student-app-remove").click(function(){
           $(".student-app-remove").unbind("click");
           $(".student-app-add").unbind("click");
            app_id = $(this).parent().attr("data-app_id");
            student_id = $(".which-student",$(this).parent()).val();
            ele = $(this).parent();
            postdata = {};
            postdata.app_id = app_id;
            postdata.student_id = student_id;
            $.ajax({
                url: "/student/removeapp",
                data: postdata,
                method: "post",
                success: function(response){
                    $(ele).replaceWith(response);
                    setUpStudentAppAdd();
                    setUpStudentAppRemove();
                }
            });
        }); 
    }
});

// Category to subcategory checkboxes
$(document).ready(function(){
    $("#AppCategory_category_id input[type=checkbox]").click(function(){
        which_category = $(this).val();
        if($(this).prop("checked") == true)
         {
             category_postdata.category_id = which_category;
             $.ajax({
                 url: "/admin/subcategory/getlist/",
                 method: "post",
                 data: category_postdata,
                 success: function(response){
                     $("#subcategory-list").append(response);
                 }
             });
         }else{
             $("#sublist-" + which_category).remove();
         }
    });
 });
 
 // View followed student app
 $(document).ready(function(){
     $("#student-followed-apps").change(function(){
         $("#submit-student-app-info-btn").unbind("click");
         postdata={}
         postdata.app_id = $(this).val();
         postdata.student_id = $(this).attr("data-student_id");
         
         $.ajax({
             url: "/student/getstudentapp",
             method: "post",
             data:  postdata,
             success: function(response){
                 $("#student-app-details").html(response);
                 $("#submit-student-app-info-btn").click(function(){
                    handleform();
                });
             }
         });
     });
     
     $("#submit-student-app-info-btn").click(function(){
         handleform();
     });
     
     function handleform(){
        postdata={};
        postdata.owned = 0;
        if( $("#student-app-info-owned").is(":checked") ){
            postdata.owned = 1;
        }
        postdata.comments = $("#student-app-info-comments").val();
        postdata.proficiency = $("#student-app-info-proficiency").val();
        postdata.student_id = $("#submit-student-app-info-btn").attr("data-student_id");
        postdata.app_id = $("#submit-student-app-info-btn").attr("data-app_id");
        
        $.ajax({
            url: "/student/updatestudentappinfo",
            method: "post",
            data: postdata,
            success: function(response){
                $("#student-app-details").html(response);
                $("#submit-student-app-info-btn").click(function(){
                    handleform();
                });
            }
        });
     }
     
    $('#slider').leanSlider({
        pauseTime: 7000, // the delay between each slide, false to turn off slideshow
        directionNav: '#slider-direction-nav',
        controlNav: '#slider-control-nav' 
    }); 
 });